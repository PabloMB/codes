# Video Game

The Video Game can be found in my other repository: IngeniaPablo
(it is the folder called OSG_ESQUELETO_VS2015)


##Instructions
Not all files are in bitbucket (since they would take too much space)  
Follow the next steps to get all the files  
**Steps 1 to 2: to get bitbucket files**  
1: Fork my repository: https://bitbucket.org/PabloMB/ingeniapablo (to your bitbucket)  
2: Pull the repository from your bitbucket  
**Step 3: to get the files that are not in bitbucket**  
3: Combine it with "lo que falta.rar" del Drive:  
https://drive.google.com/open?id=0B14iVDUZTp0LNFQxNEkxNzhsbGM  
Other link:  
https://drive.google.com/open?id=18xNMq2yZF5cDT0DdeuTZV9vWvcrErfDF  
(move files to the corresponding folder, from OSG_ESQUELETO_VS2015 in "lo que falta.rar" to OSG_ESQUELETO_VS2015 in IngeniaPablo)  
**Next steps: to execute the game**  
4: Execute file sln to open the project in Visual Studio.  
5: Make sure to be in release and x64!  
6: Execute  

If you find any problem, please write an email to pabmen3c08@gmail.com.

Thank you!

PD: you can see some videos here:  
https://drive.google.com/open?id=1oeGApyOl-qxHXMwPEXentFtimoxLx0eC  