#include <stdio.h>
#include <stdlib.h>

#define MAX 10000
#define M 1000
#define N 14

int medias[4], varianzas[4];

int sel_sort();
void cuantas_comparaciones();

int main()
{
    int array[MAX], n[N]={1,5,10,50,100,200,300,400,500,600,700,800,900,1000};
    int i=0, j=0;
    long int total[M];
    srand(time(NULL));

    FILE *g = fopen("vectortotal.txt","w+");
    for(i=0; i<N; i++)
    {
        cuantas_comparaciones(array,n[i],total);
        medias[i] = media(total);
        printf("%d\n", medias[i]);
        varianzas[i] = varianza(total, medias[i]);
        printf("%d\n", varianzas[i]);
        fprintf(g,"total%d ",n[i]);
        for(j=0;j<M;j++)
        {
            fprintf(g,"%d ",total[i]);
        }
        fprintf(g,"\n");
    }
    FILE *f = fopen("resultados.txt","w+");
    if(f==NULL)
        printf("Error al abrir el archivo");
    else
    {
        printf("Archivo abierto con exito\n");
        fprintf(f,"n medias varianzas\n");
        for(i=0; i<N; i++)
        {
            fprintf(f,"%d %d %d\n", n[i], medias[i], varianzas[i]);
        }
    }
    fclose(f);
    printf("Archivo cerrado\n");

    fclose(g);

    //Despu�s en Matlab:
    //import data
    //plot(n,medias)

    return 0;
}

int sel_sort(int *v, int n)
{
    int c,  d, position, k=0, swap;
    for(c=0; c<(n-1); c++)
    {
        position = c;

        for(d=c+1; d<n; d++)
        {
            if(v[position]>v[d])
                position = d;
            k++;
        }
        if(position!=c)
        {
           swap = v[c];
           v[c] = v[position];
           v[position] = swap;
        }
    }
    return k;
}

void cuantas_comparaciones(int *v, int n, long int *t)
{
    int i, c;
    for(i=0; i<M; i++)
    {
        for(c=0; c<n; c++)
        {
            v[c]=rand()%1001;
        }
        t[i]=sel_sort(v,n);
    }
}

int media(long int *t)
{
    int i;
    int media=0;
    for(i=0; i<M; i++)
    {
        media+=t[i];
    }
    media/=M;
}

int varianza(long int *t, int m)
{
    int i;
    int var=0;
    for(i=0; i<M; i++)
    {
        var+=(t[i]-m)*(t[i]-m);
    }
    var/=M;
}
