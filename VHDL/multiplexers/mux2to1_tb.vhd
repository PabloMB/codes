library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux2to1_tb is

end mux2to1_tb;

architecture Behavioral of mux2to1_tb is
	component mux2to1 is
		port ( A   : in std_logic;
			   B   : in std_logic;
			   sel : in std_logic;
			   X   : out std_logic);
	end component;
	signal A,B,sel,X: std_logic;
begin
	m: mux2to1 port map(A,B,sel,X);
	
	process
    begin
		
        A <= '0';
        B <= '1';
        sel <= '0';
        wait for 1 ns;
		
        A <= '0';
        B <= '1';
        sel <= '1';
        wait for 1 ns;
		
    end process;

end Behavioral;
