library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux2to1_ST_tb is

end mux2to1_ST_tb;

architecture Behavioral of mux2to1_ST_tb is
	component mux2to1_ST is
		port ( a   : in std_logic;
			   b   : in std_logic;
			   sel : in std_logic;
			   x   : out std_logic);
	end component;
signal a,b: std_logic:='0';
signal sel,x: std_logic;
begin
m: mux2to1_ST port map(a,b,sel,x);
	
	process
    begin
        wait for 2 ns;
        a <= not a;
    end process;
    
    process
    begin
        wait for 1 ns;
        b <= not b;
    end process;
    
	process
    begin
        sel <= '0';
        wait for 4 ns;
        sel <= '1';
        wait for 4 ns;
    end process;

end Behavioral;
