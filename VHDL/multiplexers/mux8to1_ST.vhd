library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux8to1_ST is
    port ( a   : in std_logic;
           b   : in std_logic;
           c   : in std_logic;
           d   : in std_logic;
           e   : in std_logic;
           f   : in std_logic;
           g   : in std_logic;
           h   : in std_logic;
		   s   : in std_logic_vector(2 downto 0);
           x   : out std_logic);
end mux8to1_ST;

architecture Structural of mux8to1_ST is
    component mux2to1_ST is
		port ( a   : in std_logic;
			   b   : in std_logic;
			   sel : in std_logic;
			   x   : out std_logic);
	end component;
signal x11,x12,x13,x14: std_logic;
signal x21,x22: std_logic;
begin
mux2to1_11: mux2to1_ST port map(a,b,s(0),x11);
mux2to1_12: mux2to1_ST port map(c,d,s(0),x12);
mux2to1_13: mux2to1_ST port map(e,f,s(0),x13);
mux2to1_14: mux2to1_ST port map(g,h,s(0),x14);

mux2to1_21: mux2to1_ST port map(x11,x12,s(1),x21);
mux2to1_22: mux2to1_ST port map(x13,x14,s(1),x22);

mux2to1_3: mux2to1_ST port map(x21,x22,s(2),x);

end Structural;
