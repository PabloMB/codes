library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux7to1_6b_ST is
    port ( a   : in std_logic_vector (5 downto 0);
           b   : in std_logic_vector (5 downto 0);
           c   : in std_logic_vector (5 downto 0);
           d   : in std_logic_vector (5 downto 0);
           e   : in std_logic_vector (5 downto 0);
           f   : in std_logic_vector (5 downto 0);
           g   : in std_logic_vector (5 downto 0);
           s   : in std_logic_vector (2 downto 0);
           x   : out std_logic_vector (5 downto 0));
end mux7to1_6b_ST;

architecture Structural of mux7to1_6b_ST is
    component mux8to1_ST is
        port ( a : in std_logic;
               b : in std_logic;
               c : in std_logic;
               d : in std_logic;
               e : in std_logic;
               f : in std_logic;
               g : in std_logic;
               h : in std_logic;
               s : in std_logic_vector (2 downto 0);
               x : out std_logic);
    end component;
begin
mux_b0: mux8to1_ST port map(a(0),b(0),c(0),d(0),e(0),f(0),g(0),'0',s,x(0));
mux_b1: mux8to1_ST port map(a(1),b(1),c(1),d(1),e(1),f(1),g(1),'0',s,x(1));
mux_b2: mux8to1_ST port map(a(2),b(2),c(2),d(2),e(2),f(2),g(2),'0',s,x(2));
mux_b3: mux8to1_ST port map(a(3),b(3),c(3),d(3),e(3),f(3),g(3),'0',s,x(3));
mux_b4: mux8to1_ST port map(a(4),b(4),c(4),d(4),e(4),f(4),g(4),'0',s,x(4));
mux_b5: mux8to1_ST port map(a(5),b(5),c(5),d(5),e(5),f(5),g(5),'0',s,x(5));

end Structural;
