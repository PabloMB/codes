library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity mux7to1_6b_BH_tb is

end mux7to1_6b_BH_tb;

architecture Behavioral of mux7to1_6b_BH_tb is
    component mux7to1_6b_BH is
        port ( a   : in std_logic_vector (5 downto 0);
               b   : in std_logic_vector (5 downto 0);
               c   : in std_logic_vector (5 downto 0);
               d   : in std_logic_vector (5 downto 0);
               e   : in std_logic_vector (5 downto 0);
               f   : in std_logic_vector (5 downto 0);
               g   : in std_logic_vector (5 downto 0);
               s   : in std_logic_vector (2 downto 0);
               x   : out std_logic_vector (5 downto 0));
    end component;
signal a,b,c,d,e,f,g,x: std_logic_vector (5 downto 0);
signal s : std_logic_vector (2 downto 0);
begin
m: mux7to1_6b_BH port map(a,b,c,d,e,f,g,s,x);
    
    process
    begin
        
        a <= "000000";
        b <= "000001";
        c <= "000011";
        d <= "000110";
        e <= "001100";
        f <= "011000";
        g <= "110000";
        s <= "000";
        wait for 1 ns;
        
        for I in 0 to 6 loop
            s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));
            wait for 1 ns;
        end loop;
        
    end process;
            
end Behavioral;
