library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity mux8to1_ST_tb is

end mux8to1_ST_tb;

architecture Behavioral of mux8to1_ST_tb is
    component mux8to1_ST is
        port ( a   : in std_logic;
               b   : in std_logic;
               c   : in std_logic;
               d   : in std_logic;
               e   : in std_logic;
               f   : in std_logic;
               g   : in std_logic;
			   h   : in std_logic;
               s   : in std_logic_vector(2 downto 0);
               x   : out std_logic);
    end component;

signal a,b,c,d,e,f,g,h,x: std_logic;
signal s: std_logic_vector (2 downto 0);
begin
m: mux8to1_ST port map(a,b,c,d,e,f,g,h,s,x);
    
    process
    begin
        a <= '0';
        b <= '0';
        c <= '0';
        d <= '0';
        e <= '0';
        f <= '0';
        g <= '0';
		h <= '0';
        
        s <= "000";
        a <= '0';
        wait for 1 ns;
        a <= '1';
        wait for 1 ns;
        
        s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));
        b <= '0';
        wait for 1 ns;
        b <= '1';
        wait for 1 ns;
        
        s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));
        c <= '0';
        wait for 1 ns;
        c <= '1';
        wait for 1 ns;
        
        s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));
        d <= '0';
        wait for 1 ns;
        d <= '1';
        wait for 1 ns;
        
        s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));
        e <= '0';
        wait for 1 ns;
        e <= '1';
        wait for 1 ns;
        
        s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));        
        f <= '0';
        wait for 1 ns;
        f <= '1';
        wait for 1 ns;
        
        s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));
        g <= '0';
        wait for 1 ns;
        g <= '1';
        wait for 1 ns;
        
        s <= std_logic_vector(to_unsigned(to_integer(unsigned( s )) + 1, 3));
        h <= '0';
        wait for 1 ns;
        h <= '1';
        wait for 1 ns;
        
    end process;
            
end Behavioral;
