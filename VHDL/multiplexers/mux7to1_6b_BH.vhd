library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux7to1_6b_BH is
    port ( a   : in std_logic_vector (5 downto 0);
           b   : in std_logic_vector (5 downto 0);
           c   : in std_logic_vector (5 downto 0);
           d   : in std_logic_vector (5 downto 0);
           e   : in std_logic_vector (5 downto 0);
           f   : in std_logic_vector (5 downto 0);
           g   : in std_logic_vector (5 downto 0);
           s : in std_logic_vector (2 downto 0);
           x   : out std_logic_vector (5 downto 0));
end mux7to1_6b_BH;

architecture Behavioral of mux7to1_6b_BH is
constant zeros: std_logic_vector (5 downto 0):="000000";
begin

    with s select x <= 
                        a when "000",
                        b when "001",
                        c when "010",
                        d when "011",
                        e when "100",
                        f when "101",
                        g when "110",
                        zeros when others;

end Behavioral;
