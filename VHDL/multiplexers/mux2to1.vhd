library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux2to1 is
    port ( a   : in std_logic;
           b   : in std_logic;
		   sel : in std_logic;
           x   : out std_logic);
end mux2to1;

architecture Behavioral of mux2to1 is
begin

    x <= a when sel='0' else b;
    
end Behavioral;
