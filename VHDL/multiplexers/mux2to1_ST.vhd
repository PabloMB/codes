library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux2to1_ST is
    port ( a   : in std_logic;
           b   : in std_logic;
		   sel : in std_logic;
           x   : out std_logic);
end mux2to1_ST;

architecture Structural of mux2to1_ST is
    component gate_NOT is
    port ( a : in std_logic;
           f : out std_logic);
    end component;
    
    component gate_AND2 is
        port ( a : in std_logic;
               b : in std_logic;
               f : out std_logic);
    end component;
    
    component gate_OR2 is
        port ( a : in std_logic;
               b : in std_logic;
               f : out std_logic);
    end component;

signal sel_L: std_logic;
signal x1,x2: std_logic;
begin
gNOT:    gate_NOT  port map(sel,sel_L);
gAND2_1: gate_AND2 port map(sel_L,a,x1);
gAND2_2: gate_AND2 port map(sel  ,b,x2);
gOR2:    gate_OR2  port map(x1,x2,x);
    
end Structural;
