library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gate_AND2 is
    port ( a : in std_logic;
           b : in std_logic;
           f : out std_logic);
end gate_AND2;

architecture Behavioral of gate_AND2 is

begin

    f <= a and b;

end Behavioral;
