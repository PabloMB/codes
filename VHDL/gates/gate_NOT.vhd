library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gate_NOT is
    port ( a : in std_logic;
           f : out std_logic);
end gate_NOT;

architecture Behavioral of gate_NOT is

begin

    f <= not a;

end Behavioral;
