library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gate_tb is

end gate_tb;

architecture Behavioral of gate_tb is
    component gate_NOT is
    port ( a : in std_logic;
           f : out std_logic);
    end component;
    
    component gate_AND2 is
        port ( a : in std_logic;
               b : in std_logic;
               f : out std_logic);
    end component;
    
    component gate_OR2 is
        port ( a : in std_logic;
               b : in std_logic;
               f : out std_logic);
    end component;

signal a,b: std_logic:='0';
signal f_NOT,f_AND2,f_OR2: std_logic;
begin
gNOT: gate_NOT port map(a,f_NOT);
gAND2: gate_AND2 port map(a,b,f_AND2);
gOR2: gate_OR2 port map(a,b,f_OR2);

    process
    begin
        wait for 2 ns;
        a <= not a;
    end process;
    
    process
    begin
        wait for 1 ns;
        b <= not b;
    end process;

end Behavioral;
