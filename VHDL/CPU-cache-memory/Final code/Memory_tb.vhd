LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY Memory_tb IS
END Memory_tb;
 
ARCHITECTURE behavior OF Memory_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Memory
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         bus_store : IN  std_logic;
         bus_load : IN  std_logic;
         bus_address : IN  std_logic_vector(31 downto 0);
         bus_word : INOUT  std_logic_vector(31 downto 0);
         bus_block : INOUT  std_logic_vector(127 downto 0);
         bus_done : OUT  std_logic;
			bus_req : OUT std_logic;
			bus_grant : IN std_logic;
			bus_busy : OUT std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal bus_store : std_logic := '0';
   signal bus_load : std_logic := '0';
   signal bus_address : std_logic_vector(31 downto 0) := (others => '0');
	signal bus_grant : std_logic;

	--BiDirs
   signal bus_word : std_logic_vector(31 downto 0);
   signal bus_block : std_logic_vector(127 downto 0);

 	--Outputs
   signal bus_done : std_logic;
	signal bus_req : std_logic;
	signal bus_busy : std_logic := '0';

   -- Clock period definitions
   constant clk_period : time := 1 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Memory PORT MAP (
          clk => clk,
          reset => reset,
          bus_store => bus_store,
          bus_load => bus_load,
          bus_address => bus_address,
          bus_word => bus_word,
          bus_block => bus_block,
          bus_done => bus_done,
			 bus_req => bus_req,
			 bus_grant => bus_grant
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
    bus_word <= (others=>'Z');
    bus_block <= (others=>'Z');
    bus_grant <= '0';
      reset <= '0';
      wait for clk_period*10;
		reset <= '1';
		wait for clk_period*10;
		bus_grant <= '1';
		bus_store <= '1';
		bus_word <= "11110000000011111111000000001111";
		bus_address <= x"000003C4"; --964
		--Should store
      --x"964" = 11110000
		--x"965" = 00001111
		--x"966" = 11110000
		--x"967" = 00001111
		
		wait for 5*clk_period;
		bus_store <= '0';
		bus_load <= '0';
		
		wait until bus_done = '1';
		
		wait for 5*clk_period;
		
		bus_store <= '1';
		bus_word <= "00000000000000000000000001010101";
		bus_address <= x"000003CC"; --972
		--Should store
      --x"972" = 00000000
		--x"973" = 00000000
		--x"974" = 00000000
		--x"975" = 01010101
		
		wait for 5*clk_period;
		bus_store <= '0';
		bus_load <= '0';
		wait until bus_done = '1';
		
		wait for 5*clk_period;
		
		bus_store <= '0';
		bus_load <= '1';
		bus_address <= x"000003C0"; --960
		
		--Should read
		--x"960" = 00000000
		--x"961" = 00000000
		--x"962" = 00000000
		--x"963" = 00000000
		--x"964" = 11110000
		--x"965" = 00001111
		--x"966" = 11110000
		--x"967" = 00001111
		--x"968" = 00000000
		--x"969" = 00000000
		--x"970" = 00000000
		--x"971" = 00000000
		--x"972" = 00000000
		--x"973" = 00000000
		--x"974" = 00000000
		--x"975" = 01010101
      wait;
   end process;

END;
