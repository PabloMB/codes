library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cacheD_tb is

end cacheD_tb;

architecture Behavioral of cacheD_tb is
    constant WORD_SIZE  : integer :=32;
    constant BLOCK_SIZE : integer :=4;
    component cacheD is
        port ( clk                   : in    std_logic;
               write_general_bus     : out   std_logic;
               bus_to_mem_address    : out   std_logic_vector (WORD_SIZE-1 downto 0);
               bus_fromto_mem_word   : inout std_logic_vector (WORD_SIZE-1 downto 0);
               bus_fromto_mem_block  : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               bus_to_mem_store      : out   std_logic;
               bus_to_mem_load       : out   std_logic;
               bus_from_mem_done     : in    std_logic;
               bus_from_cpu_Daddress : in    std_logic_vector (WORD_SIZE-1 downto 0);
               bus_fromto_cpu_data   : inout std_logic_vector (WORD_SIZE-1 downto 0);
               bus_from_cpu_store    : in    std_logic;
               bus_from_cpu_load     : in    std_logic;
               bus_to_cpu_done       : out   std_logic);
    end component;
    signal clk                   : std_logic;
    signal write_general_bus     : std_logic;
    signal bus_to_mem_address    : std_logic_vector (WORD_SIZE-1 downto 0);
    signal bus_fromto_mem_word   : std_logic_vector (WORD_SIZE-1 downto 0);
    signal bus_fromto_mem_block  : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal bus_to_mem_store      : std_logic;
    signal bus_to_mem_load       : std_logic;
    signal bus_from_mem_done     : std_logic;
    signal bus_from_cpu_Daddress : std_logic_vector (WORD_SIZE-1 downto 0);
    signal bus_fromto_cpu_data   : std_logic_vector (WORD_SIZE-1 downto 0);
    signal bus_from_cpu_store    : std_logic;
    signal bus_from_cpu_load     : std_logic;
    signal bus_to_cpu_done       : std_logic;
    
    constant clk_period : time := 1 ns;
begin
C1: cacheD port map(clk,write_general_bus,
                    bus_to_mem_address,bus_fromto_mem_word,bus_fromto_mem_block,bus_to_mem_store,bus_to_mem_load,bus_from_mem_done,
                    bus_from_cpu_Daddress,bus_fromto_cpu_data,bus_from_cpu_store,bus_from_cpu_load,
                    bus_to_cpu_done);
    
    process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;
    
    process
    begin
        bus_from_cpu_Daddress <= x"00000000";
        bus_from_cpu_load <= '0';
        bus_from_cpu_store <= '0';
        bus_fromto_cpu_data <= (others=>'Z');
        bus_fromto_mem_word <= (others=>'Z');
        bus_fromto_mem_block <= (others=>'Z');
        bus_from_mem_done <= '0';
                    
        wait for 2 ns;
        
        --read hit
        bus_from_cpu_Daddress <= x"0000" & "00" & "010101" & "0101" & "0100"; --{tag,index,offset}
        wait for 1.01 ns; --buses from CPU change after CLK changes
        bus_from_cpu_load <= '1';
        
        while (bus_to_cpu_done = '0') loop
            wait for 1 ns;
        end loop;
        
        wait for 1 ns;
        bus_from_cpu_load <= '0';
        wait for 2 ns;
        
        --read miss
        bus_from_cpu_Daddress <= x"0000" & "00" & "010101" & "0100" & "0100"; --{tag,index,offset}
        wait for 1 ns;
        bus_from_cpu_load <= '1';
        wait for 3 ns;
        
        bus_from_mem_done <= '1';
        bus_fromto_mem_block <= x"BABABABA" & x"CACACACA" & x"DADADADA" & x"EAEAEAEA";
        
        while (bus_to_cpu_done = '0') loop
            wait for 1 ns;
        end loop;
        
        wait for 1 ns;
        bus_from_cpu_load <= '0';
        wait for 3 ns;
        
        
        --write hit
        bus_from_cpu_Daddress <= x"0000" & "00" & "010101" & "0101" & "0100"; --{tag,index,offset}
        bus_fromto_cpu_data <= x"FAFAFAFA";
        wait for 1 ns; --buses from CPU change after CLK changes
        bus_from_cpu_store <= '1';
        wait for 5 ns;
        
        bus_from_mem_done <= '1';
        while (bus_to_cpu_done = '0') loop
            wait for 1 ns;
        end loop;
        
        wait for 1 ns;
        bus_from_mem_done <= '0';
        bus_from_cpu_store <= '0';
        wait for 2 ns;
        
        --write miss
        bus_from_cpu_Daddress <= x"0000" & "00" & "010101" & "0111" & "0100"; --{tag,index,offset}
        bus_fromto_cpu_data <= x"FEFEFEFE";
        wait for 1 ns;
        bus_from_cpu_store <= '1';
        wait for 3 ns;
        
        bus_from_mem_done <= '1';
        while (bus_to_cpu_done = '0') loop
            wait for 1 ns;
        end loop;
        
        wait for 1 ns;
        bus_from_mem_done <= '0';
        bus_from_cpu_store <= '0';
        wait for 2 ns;
        
    end process;
    
end Behavioral;