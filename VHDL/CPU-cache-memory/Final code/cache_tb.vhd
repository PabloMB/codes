
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cache_tb is

end cache_tb;

architecture Behavioral of cache_tb is
    constant WORD_SIZE  : integer :=32;
    constant BLOCK_SIZE : integer :=4;
    component cache is
        port ( clk                   : in    std_logic;
               BUS_address           : out   std_logic_vector (WORD_SIZE-1 downto 0);
               BUS_word              : inout std_logic_vector (WORD_SIZE-1 downto 0);
               BUS_block             : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               BUS_store             : out   std_logic;
               BUS_load              : out   std_logic;
               BUS_busy              : inout std_logic;
               BUS_done              : in    std_logic;
               MC_write_request      : out   std_logic;
               MC_write_grant        : in    std_logic;
               CPU_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);
               CPU_data              : inout std_logic_vector (WORD_SIZE-1 downto 0);
               CPU_find              : in    std_logic;
               CPU_found             : out   std_logic;
               CPU_store             : in    std_logic;
               CPU_load              : in    std_logic;
               CPU_done              : out   std_logic);
    end component;
    signal clk                   : std_logic;
    signal BUS_address           : std_logic_vector (WORD_SIZE-1 downto 0);
    signal BUS_word              : std_logic_vector (WORD_SIZE-1 downto 0);
    signal BUS_block             : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal BUS_store             : std_logic;
    signal BUS_load              : std_logic;
    signal BUS_busy              : std_logic;
    signal BUS_done              : std_logic;
    signal MC_write_request      : std_logic;
    signal MC_write_grant        : std_logic;
    signal CPU_address           : std_logic_vector (WORD_SIZE-1 downto 0);
    signal CPU_data              : std_logic_vector (WORD_SIZE-1 downto 0);
    signal CPU_find              : std_logic;
    signal CPU_found              : std_logic;
    signal CPU_store             : std_logic;
    signal CPU_load              : std_logic;
    signal CPU_done              : std_logic;
    
    constant clk_period : time := 1 ns;
begin
C1: cache port map(clk,
                    BUS_address,
                    BUS_word,
                    BUS_block,
                    BUS_store,
                    BUS_load,
                    BUS_busy,
                    BUS_done,
                    MC_write_request,
                    MC_write_grant,
                    CPU_address,
                    CPU_data,
                    CPU_find,
                    CPU_found,
                    CPU_store,
                    CPU_load,
                    CPU_done);
    
    process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;
    
    process
    begin
        
        --reset signals
        CPU_address <= (others=>'0');
        CPU_find <= '0';
        BUS_word <= (others=>'Z');
        BUS_block <= (others=>'Z');
        BUS_busy <= 'Z';
        BUS_done <= '0';
        MC_write_grant <= '0';
        CPU_address <= (others=>'0');
        CPU_data <= (others=>'Z');
        CPU_find <= '0';
        CPU_store <= '0';
        CPU_load <= '0';
        wait for 2 ns;
        
        --testing cacheI
            
            --read hit
            CPU_address <= x"0000" & "00" & "10101" & "00101" & "0100"; --{tag,index,offset}
            wait for 1.01 ns; --buses from CPU change after CLK changes
            CPU_find <= '1';
            
            while (CPU_found = '0') loop
                wait for 1 ns;
            end loop;
            
            wait for 1 ns;
            CPU_find <= '0';
            wait for 2 ns;
            
            --read miss
            CPU_address <= x"0000" & "00" & "10101" & "00100" & "0100"; --{tag,index,offset}
            wait for 1 ns;
            CPU_find <= '1';
            wait for 3 ns;
            if(MC_write_request = '1') then
                wait for 1 ns;
                MC_write_grant <= '1';
                wait for 1 ns;
                MC_write_grant <= '0';
            end if;
            
            wait for 3 ns;
            BUS_done <= '1';
            BUS_block <= x"BABABABA" & x"CACACACA" & x"DADADADA" & x"EAEAEAEA";
            
            while (CPU_found = '0') loop
                wait for 1 ns;
            end loop;
            
            wait for 1 ns;
            BUS_done <= '0';
            CPU_find <= '0';
            wait for 3 ns;
        
        BUS_block <= (others=>'Z');
        --testing cacheD
            
            --read hit
            CPU_address <= x"0000" & "00" & "010101" & "0101" & "0100"; --{tag,index,offset}
            wait for 1.01 ns; --buses from CPU change after CLK changes
            CPU_load <= '1';
            
            while (CPU_done = '0') loop
                wait for 1 ns;
            end loop;
            
            wait for 1 ns;
            CPU_load <= '0';
            wait for 2 ns;
            
            --read miss
            CPU_address <= x"0000" & "00" & "010101" & "0100" & "0100"; --{tag,index,offset}
            wait for 1 ns;
            CPU_load <= '1';
            wait for 3 ns;
            if(MC_write_request = '1') then
                wait for 1 ns;
                MC_write_grant <= '1';
                wait for 1 ns;
                MC_write_grant <= '0';
            end if;
            
            wait for 3 ns;
            BUS_done <= '1';
            BUS_block <= x"BABABABA" & x"CACACACA" & x"DADADADA" & x"EAEAEAEA";
            
            while (CPU_done = '0') loop
                wait for 1 ns;
            end loop;
            
            wait for 1 ns;
            BUS_done <= '0';
            CPU_load <= '0';
            wait for 3 ns;
            
            
            --write hit
            CPU_address <= x"0000" & "00" & "010101" & "0101" & "0100"; --{tag,index,offset}
            CPU_data <= x"FAFAFAFA";
            wait for 1 ns;
            CPU_store <= '1';
            wait for 3 ns;
            if(MC_write_request = '1') then
                wait for 1 ns;
                MC_write_grant <= '1';
                wait for 1 ns;
                MC_write_grant <= '0';
            end if;
            
            wait for 3 ns;
            BUS_done <= '1';
            while (CPU_done = '0') loop
                wait for 1 ns;
            end loop;
            
            wait for 1 ns;
            BUS_done <= '0';
            CPU_store <= '0';
            wait for 2 ns;
            
            --write miss
            CPU_address <= x"0000" & "00" & "010101" & "0111" & "0100"; --{tag,index,offset}
            CPU_data <= x"FEFEFEFE";
            wait for 1 ns;
            CPU_store <= '1';
            wait for 3 ns;
            if(MC_write_request = '1') then
                wait for 1 ns;
                MC_write_grant <= '1';
                wait for 1 ns;
                MC_write_grant <= '0';
            end if;
            
            wait for 3 ns;
            BUS_done <= '1';
            while (CPU_done = '0') loop
                wait for 1 ns;
            end loop;
            
            wait for 1 ns;
            BUS_done <= '0';
            CPU_store <= '0';
            wait for 2 ns;
        
    end process;
    
end Behavioral;