

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity cacheController_tb is

end cacheController_tb;

architecture Behavioral of cacheController_tb is
    constant WORD_SIZE  : integer :=32;
    constant BLOCK_SIZE : integer :=4;
    component cacheController is
        port ( clk                  : in    std_logic;
               CI_write_request     : in    std_logic;                                        --CI = cache I
               CI_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);          --CD = cache D
               CI_word              : out   std_logic_vector (WORD_SIZE-1 downto 0);          --MC = main controller
               CI_block             : out   std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               CI_found             : out   std_logic;
               CD_write_request     : in    std_logic;
               CD_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);
               CD_word              : inout std_logic_vector (WORD_SIZE-1 downto 0);
               CD_block             : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               CD_store             : in    std_logic;
               CD_load              : in    std_logic;
               BUS_address          : out   std_logic_vector (WORD_SIZE-1 downto 0);
               BUS_word             : inout std_logic_vector (WORD_SIZE-1 downto 0);
               BUS_block            : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               BUS_store            : out   std_logic;
               BUS_load             : out   std_logic;
               BUS_busy             : inout std_logic;
               BUS_done             : in    std_logic;
               MC_write_request     : out   std_logic;
               MC_write_grant       : in    std_logic);
    end component;
    signal clk                 : std_logic;
    signal CI_write_request    : std_logic;                                          --CI = cache I
    signal CI_address          : std_logic_vector (WORD_SIZE-1 downto 0);            --CD = cache D
    signal CI_word             : std_logic_vector (WORD_SIZE-1 downto 0);            --MC = main controller
    signal CI_block            : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal CI_found            : std_logic;
    signal CD_write_request    : std_logic;
    signal CD_address          : std_logic_vector (WORD_SIZE-1 downto 0);
    signal CD_word             : std_logic_vector (WORD_SIZE-1 downto 0);
    signal CD_block            : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal CD_store            : std_logic;
    signal CD_load             : std_logic;
    signal BUS_address         : std_logic_vector (WORD_SIZE-1 downto 0);
    signal BUS_word            : std_logic_vector (WORD_SIZE-1 downto 0);
    signal BUS_block           : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal BUS_store           : std_logic;
    signal BUS_load            : std_logic;
    signal BUS_busy            : std_logic;
    signal BUS_done            : std_logic;
    signal MC_write_request    : std_logic;
    signal MC_write_grant      : std_logic;
    
    constant clk_period : time := 1 ns;
begin
CC1: cacheController port map(clk,CI_write_request,CI_address,CI_word,CI_block,CI_found,
                                  CD_write_request,CD_address,CD_word,CD_block,CD_store,CD_load,
                                  BUS_address,BUS_word,BUS_block,BUS_store,BUS_load,BUS_busy,BUS_done,
                                  MC_write_request,MC_write_grant);
    
    process
    begin
    clk <= '1';
    wait for clk_period/2;
    clk <= '0';
    wait for clk_period/2;
    end process;
    
    process
    begin
        --reset every signal 
        CI_write_request <= '0';
        CD_write_request <= '0';
        CI_address <= (others=>'0');
        CD_address <= (others=>'0');
        CD_store <= '0';
        CD_load  <= '0';     
        BUS_done <= '0';
        BUS_word <= (others=>'Z');
        BUS_block <= (others=>'Z');
        CD_word <= (others=>'Z');
        CD_block <= (others=>'Z');
        BUS_busy <= 'Z';
        MC_write_grant <= '0';
        wait for 2.01 ns; --buses change after CLK changes
        
        --cacheI loading instruction from mem 
        CI_write_request <= '1';
        CI_address <= x"0000" & "00" & "10101" & "00101" & "0100";
        wait for 1 ns;
        while (MC_write_request = '0') loop
            wait for 1 ns;
        end loop;
        wait for 1 ns;
        MC_write_grant <= '1';
        wait for 1 ns;
        MC_write_grant <= '0';
        CI_write_request <= '0';
        wait for 5 ns;
        BUS_done <= '1';
        BUS_word <= x"FAFAFAFA";
        BUS_block <= x"BABABABA" & x"CACACACA" & x"DADADADA" & x"EAEAEAEA";
        wait for 3 ns;
        
        
        
        --reset every signal 
        CI_write_request <= '0';
        CD_write_request <= '0';
        CI_address <= (others=>'0');
        CD_address <= (others=>'0');
        CD_store <= '0';
        CD_load  <= '0';     
        BUS_done <= '0';
        BUS_word <= (others=>'Z');
        BUS_block <= (others=>'Z');
        CD_word <= (others=>'Z');
        CD_block <= (others=>'Z');
        BUS_busy <= 'Z';
        MC_write_grant <= '0';
        wait for 2 ns;
        
        --cacheD loading data from mem 
        CD_write_request <= '1';
        CD_address <= x"0000" & "00" & "10101" & "00101" & "0100";
        CD_load <= '1';
        wait for 1 ns;
        while (MC_write_request = '0') loop
            wait for 1 ns;
        end loop;
        wait for 1 ns;
        MC_write_grant <= '1';
        wait for 1 ns;
        MC_write_grant <= '0';
        CD_write_request <= '0';
        wait for 5 ns;
        BUS_done <= '1';
        BUS_word <= x"CCCCCCCC";
        BUS_block <= x"CCCCCCCC" & x"AAAAAAAA" & x"CCCCCCCC" & x"AAAAAAAA";
        wait for 3 ns;
        
        
        
        --reset every signal 
        CI_write_request <= '0';
        CD_write_request <= '0';
        CI_address <= (others=>'0');
        CD_address <= (others=>'0');
        CD_store <= '0';
        CD_load  <= '0';     
        BUS_done <= '0';
        BUS_word <= (others=>'Z');
        BUS_block <= (others=>'Z');
        CD_word <= (others=>'Z');
        CD_block <= (others=>'Z');
        BUS_busy <= 'Z';
        MC_write_grant <= '0';
        wait for 2 ns;
        
        --cacheD storing data in mem 
        CD_write_request <= '1';
        CD_address <= x"0000" & "00" & "10101" & "00101" & "0100";
        CD_store <= '1';
        wait for 1 ns;
        while (MC_write_request = '0') loop
            wait for 1 ns;
        end loop;
        wait for 1 ns;
        MC_write_grant <= '1';
        wait for 1 ns;
        MC_write_grant <= '0';
        CD_write_request <= '0';
        wait for 5 ns;
        BUS_done <= '1';
        BUS_word <= x"BBBBBBBB";
        BUS_block <= x"BBBBBBBB" & x"AAAAAAAA" & x"BBBBBBBB" & x"AAAAAAAA";
        wait for 3 ns;
        
        
        
    end process;

end Behavioral;
