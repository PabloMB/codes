library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cacheController is   --controls who writes in the general bus 
    generic ( WORD_SIZE  : integer :=32;
              BLOCK_SIZE : integer :=4 );
    port ( clk                  : in    std_logic;
           CI_write_request     : in    std_logic;                                              --CI = cache I
           CI_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);                --CD = cache D
           CI_word              : out   std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0'); --MC = main controller
           CI_block             : out   std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0):=(others=>'0');
           CI_found             : out   std_logic:='0';
           CD_write_request     : in    std_logic;
           CD_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);
           CD_word              : inout std_logic_vector (WORD_SIZE-1 downto 0);
           CD_block             : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
           CD_store             : in    std_logic;
           CD_load              : in    std_logic;
           CD_done              : out   std_logic:='0';
           BUS_address          : out   std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
           BUS_word             : inout std_logic_vector (WORD_SIZE-1 downto 0);
           BUS_block            : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
           BUS_store            : out   std_logic:='0';
           BUS_load             : out   std_logic:='0';
           BUS_busy             : inout std_logic;
           BUS_done             : in    std_logic;
           MC_write_request     : out   std_logic:='0';
           MC_write_grant       : in    std_logic);
end cacheController;

architecture Behavioral of cacheController is
    type state_type is (IDLE, CACHE_I_TOWRITE, CACHE_I_WRITING, CACHE_I_WAITING_ANSWER, CACHE_I_READ_ANSWER,
                              CACHE_D_TOWRITE, CACHE_D_WRITING, CACHE_D_WAITING_ANSWER, CACHE_D_READ_ANSWER);
    signal state : state_type :=IDLE;
    
    signal CI_attended      : std_logic :='0';
    --signal CI_address_tosend : std_logic_vector (WORD_SIZE-1 downto 0);
    
    signal CD_attended      : std_logic :='0';
    --signal CD_address_tosend : std_logic_vector (WORD_SIZE-1 downto 0);
    
    --signals to control inout (bidirectional) ports
    signal BUS_block_Write  : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0):=(others=>'0');
    signal BUS_block_Read   : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal en_BUS_block     : std_logic :='1';
    
    signal BUS_word_Write  : std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
    signal BUS_word_Read   : std_logic_vector (WORD_SIZE-1 downto 0);
    signal en_BUS_word     : std_logic :='1';
    
    signal CD_block_Write  : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0):=(others=>'0');
    signal CD_block_Read   : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal en_CD_block     : std_logic :='1';
    
    signal CD_word_Write  : std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
    signal CD_word_Read   : std_logic_vector (WORD_SIZE-1 downto 0);
    signal en_CD_word     : std_logic :='1';
    
    signal BUS_busy_Write  : std_logic :='1';
    signal BUS_busy_Read   : std_logic;
    signal en_BUS_busy     : std_logic :='1';
begin
    
    process(clk)
    begin
        if clk'event and clk='1' then
            
            --when cacheI wants to write
            if(state = IDLE and CI_write_request = '1' and CI_attended = '0') then
                state <= CACHE_I_TOWRITE;
                CI_attended <= '1';
            
            elsif(state = CACHE_I_TOWRITE and MC_write_grant = '1') then                
                state <= CACHE_I_WRITING;
                en_BUS_busy <= '0';
                BUS_busy_Write <= '1';
            
            elsif(state = CACHE_I_WRITING) then    --only one cycle to write in bus            
                state <= CACHE_I_WAITING_ANSWER;
                en_BUS_busy <= '1';

            elsif(state = CACHE_I_WAITING_ANSWER and BUS_done = '1') then
                state <= CACHE_I_READ_ANSWER;
                
            elsif(state = CACHE_I_READ_ANSWER) then    --only one cycle to write in bus            
                state <= IDLE;
            
            
            --when cacheD wants to write
            elsif(state = IDLE and CD_write_request = '1' and CD_attended = '0') then
                state <= CACHE_D_TOWRITE;
                CD_attended <= '1';
            
            elsif(state = CACHE_D_TOWRITE and MC_write_grant = '1') then                
                state <= CACHE_D_WRITING;
                --BUS_busy <= '1';
                en_BUS_busy <= '0';
                BUS_busy_Write <= '1';
            
            elsif(state = CACHE_D_WRITING) then    --only one cycle to write in bus            
                state <= CACHE_D_WAITING_ANSWER;
                --BUS_busy <= '0';
                en_BUS_busy <= '1';

            elsif(state = CACHE_D_WAITING_ANSWER and BUS_done = '1') then
                state <= CACHE_D_READ_ANSWER;
                
            elsif(state = CACHE_D_READ_ANSWER) then    --only one cycle to write in bus            
                state <= IDLE;
            
            end if;
            
            if(CI_write_request = '0') then
                CI_attended <= '0';
            end if;
            if(CD_write_request = '0') then
                CD_attended <= '0';
            end if;
            
        end if;
    end process;
    
    process(state)
    begin
        case state is
            when CACHE_I_TOWRITE =>
                MC_write_request <= '1';
            when CACHE_I_WRITING =>
                MC_write_request <= '0';
                BUS_address <= CI_address;
                BUS_load <= '1';
                BUS_store <= '0';
            when CACHE_I_WAITING_ANSWER =>
                BUS_load <= '0';
                en_BUS_block <= '1'; --set en_BUS_block to allow others to write on inout BUS_block and later read it
                en_BUS_word  <= '1'; --same with BUS_word
            when CACHE_I_READ_ANSWER =>
                CI_word  <= BUS_word_Read;
                CI_block <= BUS_block_Read;
                CI_found <= BUS_done;
                
            when CACHE_D_TOWRITE =>
                MC_write_request <= '1';
            when CACHE_D_WRITING =>
                MC_write_request <= '0';
                BUS_address <= CD_address;
                en_BUS_word <= '0';
                en_BUS_block <= '0';
                BUS_word_Write <= CD_word_Read;
                BUS_block_Write <= CD_block_Read;
                BUS_load <= CD_load;
                BUS_store <= CD_store;
            when CACHE_D_WAITING_ANSWER => --(if any)
                BUS_load <= '0';
                BUS_store <= '0';
                en_BUS_block <= '1'; --set en_BUS_block to allow others to write on inout BUS_block and later read it
                en_BUS_word  <= '1'; --same with BUS_word
            when CACHE_D_READ_ANSWER =>
                CD_word_Write  <= BUS_word_Read;
                CD_block_Write <= BUS_block_Read;
                CD_done <= '1';
                
            when others =>
                MC_write_request <= '0';
                en_BUS_block <= '1';
                en_BUS_word  <= '1';
                CD_done <= '0';
                CI_found <= '0';
        end case;
    end process;
    
    
    
    BUS_block <= (others=>'Z') when en_BUS_block = '1'  --set en_BUS_block to be able to read inout BUS_block
                  else BUS_block_Write;
    BUS_block_Read <= BUS_block;
    
    BUS_word <= (others=>'Z') when en_BUS_word = '1'  --set en_BUS_word to be able to read inout BUS_word
                 else BUS_word_Write;
    BUS_word_Read <= BUS_word;
    
    CD_block <= (others=>'Z') when en_CD_block = '1'  --set en_CD_block to be able to read inout CD_block
                 else CD_block_Write;
    CD_block_Read <= CD_block;
    
    CD_word <= (others=>'Z') when en_CD_word = '1'  --set en_CD_word to be able to read inout CD_word
                else CD_word_Write;
    CD_word_Read <= CD_word;
    
    BUS_busy <= '0' when en_BUS_busy = '1'  --set en_BUS_busy to be able to read inout BUS_busy
                else BUS_busy_Write;
    BUS_busy_Read <= BUS_busy;
    
    
    en_CD_word <= '1' when CD_store = '1' else
                  '0' when CD_load = '1' else
                  '1';
                  
    en_CD_block <= '1' when CD_store = '1' else
                   '0' when CD_load = '1' else
                   '1';
    
end Behavioral;
