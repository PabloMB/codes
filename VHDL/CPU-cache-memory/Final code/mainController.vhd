library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mainController is
    port ( clk      : in  std_logic;
           bus_busy1: in  std_logic;
           bus_busy2: in  std_logic;
           request1 : in  std_logic;
           grant1   : out std_logic;
           request2 : in  std_logic;
           grant2   : out std_logic);
end mainController;

architecture Behavioral of mainController is

    --signal request1_attended : std_logic :='0'; --not needed
    signal request2_attended : std_logic :='0';
begin

    process(clk)
    begin
        if clk'event and clk='1' then
        
            if((bus_busy1 or bus_busy2) = '0') then
                if(request1 = '1' and request2_attended = '0') then
                    grant1 <= '1';
                else
                    grant1 <= '0';
                    if(request2 = '1') then
                        grant2 <= '1';
                        request2_attended <= '1';
                    else
                        grant2 <= '0';
                        request2_attended <= '0';
                    end if;
                end if;
            else
                grant1 <= '0';
                grant2 <= '0';
            end if;
        
        end if;
    end process;

end Behavioral;
