library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Memory is
	generic(
		CLOCK_CYCLE	: time := 1 ns
	);
	port(
		clk			: in std_logic;
		reset 		: in std_logic;
		bus_store	: in std_logic;
		bus_load		: in std_logic;
		bus_grant	: in std_logic;
		bus_address	: in std_logic_vector(31 downto 0);
		
		bus_word		: inout std_logic_vector(31 downto 0);
		bus_block	: inout std_logic_vector(127 downto 0);
		
		bus_done		: out std_logic;
		bus_req		: out std_logic;
		bus_busy		: out	std_logic
		
	);
	end Memory;

architecture Behavioral of Memory is

	type memtable is array(4095 downto 0) of std_logic_vector(7 downto 0); --4096 positions of 8 bits each
		
		function clear_mem return memtable is
			variable memfile : memtable;
		begin
			for I in 4095 downto 1024 loop
				memfile(I) := (others => '0');
			end loop;
			for I in 1019 downto 516 loop
				memfile(I) := (others => '0');
			end loop;
			for I in 503 downto 264 loop
				memfile(I) := (others => '0');
			end loop;
			for I in 259 downto 4 loop
				memfile(I) := (others => '0');
			end loop;
			
			--Add R3,R5,R7
			memfile(260) := "00000000";
			memfile(261) := "10100111";
			memfile(262) := "00011000";
			memfile(263) := "00100000";
			
			--Load R7,1023(R0)
			memfile(504) := "10001100";
			memfile(505) := "00000111";
			memfile(506) := "00000011";
			memfile(507) := "11111111";
			
			--Load R5,0(R0)
			memfile(508) := "10001100";
			memfile(509) := "00000101";
			memfile(510) := "00000000";
			memfile(511) := "00000000";
			
			--J 260
			memfile(512) := "00001000";
			memfile(513) := "00000000";
			memfile(514) := "00000000";
			memfile(515) := "01000001";
			
			--16711935
			memfile(1020) := "00000000";
			memfile(1021) := "11111111";
			memfile(1022) := "00000000";
			memfile(1023) := "11111111";
			
			--1
			memfile(0) := "00000000";
			memfile(1) := "00000000";
			memfile(2) := "00000000";
			memfile(3) := "00000001";
			
			return memfile;
		end function;
		
	signal mem_map : memtable := clear_mem;
	
	type STATES is (idle, load_req, load_grant, store_req, store_grant);
	signal CurrentState : STATES := idle;

begin


	process(clk, reset)
	begin
	
		if (reset = '0') then
			mem_map <= clear_mem;
			bus_done <= '0';
			bus_word <= (others => 'Z');
			bus_block <= (others => 'Z');
			bus_req <= '0';
			bus_done <= '0';
			bus_busy <= '0';
		else
			if (falling_edge(clk)) then
				case CurrentState is
					when idle =>
						bus_done <= '0';
						bus_busy <= '0';
						bus_req <=	'0';
						bus_word <= (others => 'Z');
						--bus_block <= (others => 'Z');
						if (bus_load = '1') then
							CurrentState <= load_req;
						elsif (bus_store = '1') then
							CurrentState <= store_req;
						end if;
					
					when load_req =>
						bus_req <= '1';
						bus_busy <= '0';
						if (bus_grant = '1') then
							CurrentState <= load_grant;
						end if;
					
					when load_grant =>
						bus_req <= '0';
						bus_busy <= '1';
						bus_word <= (others => 'Z');
						
						bus_block(127 downto 120) <= mem_map(conv_integer((bus_address and x"FFFFFFF0"))) 			after (4 * CLOCK_CYCLE);
						bus_block(119 downto 112) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 1) 		after (6 * CLOCK_CYCLE);
						bus_block(111 downto 104) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 2) 		after (8 * CLOCK_CYCLE);
						bus_block(103 downto 96) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 3) 		after (10 * CLOCK_CYCLE);
						bus_block(95 downto 88) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 4) 		after (12 * CLOCK_CYCLE);
						bus_block(87 downto 80) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 5) 		after (14 * CLOCK_CYCLE);
						bus_block(79 downto 72) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 6) 		after (16 * CLOCK_CYCLE);
						bus_block(71 downto 64) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 7) 		after (18 * CLOCK_CYCLE);
						bus_block(63 downto 56) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 8) 		after (20 * CLOCK_CYCLE);
						bus_block(55 downto 48) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 9) 		after (22 * CLOCK_CYCLE);
						bus_block(47 downto 40) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 10) 		after (24 * CLOCK_CYCLE);
						bus_block(39 downto 32) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 11) 		after (26 * CLOCK_CYCLE);
						bus_block(31 downto 24) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 12) 		after (28 * CLOCK_CYCLE);
						bus_block(23 downto 16) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 13) 		after (30 * CLOCK_CYCLE);
						bus_block(15 downto 8) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 14) 		after (32 * CLOCK_CYCLE);
						bus_block(7 downto 0) <= mem_map(conv_integer((bus_address and x"FFFFFFF0")) + 15) 			after (34 * CLOCK_CYCLE);
						
						bus_done <= '1' after (34 * CLOCK_CYCLE);
						CurrentState <= idle after (35 * CLOCK_CYCLE);
					
					when store_req =>
						bus_req <= '1';
						bus_busy <= '0';
							if (bus_grant = '1') then
								CurrentState <= store_grant;
							end if;
					
					when store_grant =>
						bus_req <= '0';
						bus_busy <= '1';
						mem_map(conv_integer((bus_address and x"FFFFFFFC"))) <= bus_word(31 downto 24)			after (8 * CLOCK_CYCLE);
						mem_map(conv_integer((bus_address and x"FFFFFFFC")) + 1) <= bus_word(23 downto 16)		after (11 * CLOCK_CYCLE);
						mem_map(conv_integer((bus_address and x"FFFFFFFC")) + 2) <= bus_word(15 downto 8)		after (14 * CLOCK_CYCLE);
						mem_map(conv_integer((bus_address and x"FFFFFFFC")) + 3) <= bus_word(7 downto 0)			after (17 * CLOCK_CYCLE);
					
						bus_done <= '1' after (17 * CLOCK_CYCLE);
						CurrentState <= idle after (18 * CLOCK_CYCLE);
					when others =>
					
				end case;
			end if;	
		end if;	
	end process;


end Behavioral;

