library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;


entity CPU is
	port(
		clk	: in	std_logic;
		reset	: in	std_logic;
		
		bus_fromto_cpu_data	:	inout		std_logic_vector(31 downto 0) := (others => 'Z');
		bus_to_cpu_found		:	in			std_logic;
		cache_done				:	in			std_logic;
		
		bus_from_cpu_address	:	out		std_logic_vector(31 downto 0);
		bus_from_cpu_find		:	out		std_logic;
		cache_store				:	out		std_logic;
		cache_load				:	out		std_logic
	);
end CPU;

architecture Behavioral of CPU is

--	constant Ctime			: time :=	20 ns; --Clock time = 20 ns

	type regstable is array(31 downto 0) of std_logic_vector(31 downto 0); --32 registers of 32 bits each
		
		function clear_registers return regstable is
			variable regfile : regstable;
		begin
			for I in 31 downto 0 loop
				regfile(I) := (others => '0');
			end loop;
			return regfile;
		end function;
		
	signal registers : regstable := clear_registers;

	constant LW_OPCODE	:	std_logic_vector(31 downto 26)	:=	"100011";
	constant SW_OPCODE	:	std_logic_vector(31 downto 26)	:=	"101011";
	constant ALU_OPCODE	:	std_logic_vector(31 downto 26)	:=	"000000";
		constant ADD_FUNC		:	std_logic_vector(5 downto 0)		:=	"100000";
		constant NOR_FUNC		:	std_logic_vector(5 downto 0)		:= "100111"; --http://alumni.cs.ucr.edu/~vladimir/cs161/mips.html
		constant SLL_FUNC		:	std_logic_vector(5 downto 0)		:= "000000"; --http://alumni.cs.ucr.edu/~vladimir/cs161/mips.html
	constant BEQ_OPCODE	:	std_logic_vector(31 downto 26)	:=	"000100";
	constant XORI_OPCODE	:	std_logic_vector(31 downto 26)	:=	"001101";
	constant J_OPCODE		:	std_logic_vector(31 downto 26)	:=	"000010";
	constant JR_OPCODE	:	std_logic_vector(31 downto 26)	:=	"001000"; --http://alumni.cs.ucr.edu/~vladimir/cs161/mips.html

	signal PC	:	std_logic_vector(31 downto 0); --4096 address, 1024 words
	signal IR	:	std_logic_vector(31 downto 0);
	signal MDR	:	std_logic_vector(31 downto 0); --Memory Data Register
	
	signal A		: std_logic_vector(4 downto 0);
	signal B		: std_logic_vector(4 downto 0);
	signal AluOutput : std_logic_vector(31 downto 0)	:= (others => '0');
	
	
	alias OPCODE	:	std_logic_vector(31 downto 26)	is	IR(31 downto 26);
	alias RS			:	std_logic_vector(25 downto 21)	is	IR(25 downto 21);
	alias RT      	: 	std_logic_vector(20 downto 16)	is IR(20 downto 16);
   alias RD      	: 	std_logic_vector(15 downto 11) 	is IR(15 downto 11);
   alias SHAMT   	: 	std_logic_vector(10 downto 6)  	is IR(10 downto 6);
   alias FUNC    	: 	std_logic_vector(5  downto 0)  	is IR(5 downto 0);
   alias IMM     	: 	std_logic_vector(15 downto 0)  	is IR(15 downto 0);
	alias ADDRESS 	: 	std_logic_vector(25 downto 0) 	is IR(25 downto 0);

	type STATES is (InF, RR, EX, MEM, WB);
	signal CurrentState : STATES := InF;

begin

	process(clk, reset)
--	variable temp	:	std_logic_vector(15 downto 0) := (others => '0');
	begin
	
	if (reset = '0') then
		PC <= x"000001F8"; --504
		IR <= std_logic_vector(to_unsigned(0, IR'length));
		bus_from_cpu_address <= std_logic_vector(to_unsigned(0, bus_from_cpu_address'length));
		bus_from_cpu_find <= '0';
		cache_store <= '0';
		cache_load <= '0';
		Currentstate <= InF;
	else
		if falling_edge(clk) then
			case CurrentState is
				when InF => 	-------------------------------------------------
					if(bus_to_cpu_found = '0') then
					bus_from_cpu_address <= PC;
					bus_from_cpu_find <= '1';
					
					else
					IR <= bus_fromto_cpu_data;
					CurrentState <= RR;
					bus_from_cpu_find <= '0';
					PC <= PC + 4;
					end if;
				
				when RR => 		-------------------------------------------------
					
					A <= RS;
					B <= RT;
					CurrentState <= EX;
					
					
				when EX => 			-------------------------------------------------
					
					case OPCODE is
						
						when ALU_OPCODE =>
							case FUNC is
								when ADD_FUNC =>
									AluOutput <= registers(conv_integer(A)) + registers(conv_integer(B));
								
								when NOR_FUNC =>
									AluOutput <= registers(conv_integer(A)) nor registers(conv_integer(B));

								when SLL_FUNC =>
									AluOutput <= to_stdlogicvector(to_bitvector(registers(conv_integer(A))) sll conv_integer(SHAMT)); --sll only works with booloan, bit and ulogic
								
								when others =>
							end case;
							CurrentState <= MEM;

						when BEQ_OPCODE =>
							if (registers(conv_integer(A)) = registers(conv_integer(B))) then
								if (IMM(15) = '0') then
									PC <= PC + (IMM & "00"); 
								else
									PC <= PC - ((not IMM + 1) & "00");
								end if;
							end if;
							CurrentState <= MEM;
							
						when XORI_OPCODE =>
							registers(conv_integer(B)) <= registers(conv_integer(A)) xor IMM;
							CurrentState <= MEM;

						when J_OPCODE =>	
							PC <= (31 downto 18 => '0') & IMM & "00";
							CurrentState <= MEM;
						
						when JR_OPCODE =>
							if registers(conv_integer(A))(31) = '0' then
								PC <= PC + registers(conv_integer(A));
							else
								PC <= PC - (not registers(conv_integer(A)) + 1);
							end if;
							CurrentState <= MEM;
							
						when others =>
							--Do nothing
							CurrentState <= MEM;
					end case;
					
					
				when MEM => 				-------------------------------------------------
					case OPCODE is
						when LW_OPCODE =>
								if(bus_to_cpu_found = '0') then
									bus_from_cpu_address <= registers(conv_integer(A)) + IMM;
									bus_from_cpu_find <= '1';
								else
									MDR <= bus_fromto_cpu_data;
									bus_from_cpu_find <= '0';
									CurrentState <= WB;
								end if;
								cache_load <= '1';
								
							when SW_OPCODE =>
								if (bus_to_cpu_found = '0') then
									bus_from_cpu_address <= registers(conv_integer(A)) + IMM;
									bus_from_cpu_find <= '0';
									bus_fromto_cpu_data <= (31 downto B'length => '0') & B;
								else 
									bus_fromto_cpu_data <= (others => 'Z');
									CurrentState <= WB;
								end if;
								cache_store <= '1';
								
							when others =>
								CurrentState <= WB;
						end case;
					
					
					
				when WB => 			-------------------------------------------------
					case OPCODE is
						when ALU_OPCODE =>
							registers(conv_integer(RD)) <= AluOutput;
						
						when LW_OPCODE =>
							registers(conv_integer(B)) <= MDR;
							
						when others =>
					end case;
					cache_load <= '0';
					cache_store <= '0';
					CurrentState <= InF;
			end case;
		end if;
	end if;
	end process;
	
	


end Behavioral;

