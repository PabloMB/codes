
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cacheI_tb is

end cacheI_tb;

architecture Behavioral of cacheI_tb is
    component cacheI is
        port ( clk                  : in  std_logic;
               write_general_bus    : out std_logic;
               bus_to_mem_address   : out std_logic_vector (31 downto 0);
               bus_from_mem_word    : in  std_logic_vector (31 downto 0);
               bus_from_mem_block   : in  std_logic_vector (32*4-1 downto 0);
               bus_from_mem_found   : in  std_logic;
               bus_from_cpu_address : in  std_logic_vector (31 downto 0);
               bus_to_cpu_data      : out std_logic_vector (31 downto 0);
               bus_from_cpu_find    : in  std_logic;
               bus_to_cpu_found     : out std_logic);
    end component;
    signal clk                  : std_logic;
    signal write_general_bus    : std_logic;
    signal bus_to_mem_address   : std_logic_vector (31 downto 0);
    signal bus_from_mem_word    : std_logic_vector (31 downto 0);
    signal bus_from_mem_block   : std_logic_vector (32*4-1 downto 0);
    signal bus_from_mem_found   : std_logic;
    signal bus_from_cpu_address : std_logic_vector (31 downto 0);
    signal bus_to_cpu_data      : std_logic_vector (31 downto 0);
    signal bus_from_cpu_find    : std_logic;
    signal bus_to_cpu_found     : std_logic;
    
    constant clk_period : time := 1 ns;
begin
C1: cacheI port map(clk,write_general_bus,bus_to_mem_address,bus_from_mem_word,bus_from_mem_block,bus_from_mem_found,
                    bus_from_cpu_address,bus_to_cpu_data,bus_from_cpu_find,bus_to_cpu_found);
    
    process
    begin
    clk <= '1';
    wait for clk_period/2;
    clk <= '0';
    wait for clk_period/2;
    end process;
    
    process
    begin
        bus_from_cpu_address <= (others=>'0');
        bus_from_mem_word <= (others=>'0');
        bus_from_mem_block <= (others=>'0');
        bus_from_cpu_find <= '0';
        bus_from_mem_found <= '0';
                        
        wait for 2 ns;
        
        --read hit
        bus_from_cpu_address <= x"0000" & "00" & "10101" & "00101" & "0100"; --{tag,index,offset}
        wait for 1.01 ns; --buses from CPU change after CLK changes
        bus_from_cpu_find <= '1';
        
        while (bus_to_cpu_found = '0') loop
            wait for 1 ns;
        end loop;
        
        wait for 1 ns;
        bus_from_cpu_find <= '0';
        wait for 2 ns;
        
        --read miss
        bus_from_cpu_address <= x"0000" & "00" & "10101" & "00100" & "0100"; --{tag,index,offset}
        wait for 1 ns;
        bus_from_cpu_find <= '1';
        wait for 3 ns;
        
        bus_from_mem_found <= '1';
        bus_from_mem_block <= x"BABABABA" & x"CACACACA" & x"DADADADA" & x"EAEAEAEA";
        
        while (bus_to_cpu_found = '0') loop
            wait for 1 ns;
        end loop;
        
        wait for 1 ns;
        bus_from_cpu_find <= '0';
        wait for 3 ns;
    end process;
    
end Behavioral;
