library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP_MODULE is
	port (
		clk	: in std_logic;
		reset	: in std_logic
	);
end TOP_MODULE;

architecture Behavioral of TOP_MODULE is



	component CPU is
		port(
		clk	: in	std_logic;
		reset	: in	std_logic;
		
		bus_fromto_cpu_data	:	inout		std_logic_vector(31 downto 0) := (others => 'Z');
		bus_to_cpu_found		:	in			std_logic;
		cache_done				:	in			std_logic;
		
		bus_from_cpu_address	:	out		std_logic_vector(31 downto 0);
		bus_from_cpu_find		:	out		std_logic;
		cache_store				:	out		std_logic;
		cache_load				:	out		std_logic
	);
	end component;
		
	
	
	
	component cache is
		generic ( WORD_SIZE  : integer :=32;
              BLOCK_SIZE : integer :=4 );
    port ( clk                   : in    std_logic;
           BUS_address           : out   std_logic_vector (WORD_SIZE-1 downto 0);
           BUS_word              : inout std_logic_vector (WORD_SIZE-1 downto 0);
           BUS_block             : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
           BUS_store             : out   std_logic;
           BUS_load              : out   std_logic;
           BUS_busy              : inout std_logic;
           BUS_done              : in    std_logic;
           MC_write_request      : out   std_logic;
           MC_write_grant        : in    std_logic;
           CPU_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);
           CPU_data              : inout std_logic_vector (WORD_SIZE-1 downto 0);
           CPU_find              : in    std_logic;
           CPU_found             : out   std_logic;
           CPU_store             : in    std_logic;
           CPU_load              : in    std_logic;
           CPU_done              : out   std_logic);
	end component;
	
	
	
	component Memory is
		generic(
			CLOCK_CYCLE	: time := 1 ns
		);
		port(
			clk			: in std_logic;
			reset 		: in std_logic;
			bus_store	: in std_logic;
			bus_load		: in std_logic;
			bus_grant	: in std_logic;
			bus_address	: in std_logic_vector(31 downto 0);
			
			bus_word		: inout std_logic_vector(31 downto 0);
			bus_block	: inout std_logic_vector(127 downto 0);
			
			bus_done		: out std_logic;
			bus_req		: out std_logic;
			bus_busy		: out	std_logic
		);
	end component;


	component mainController is
		port ( clk     : in  std_logic;
           bus_busy1: in  std_logic;
			  bus_busy2: in  std_logic;
           request1 : in  std_logic;
           grant1   : out std_logic;
           request2 : in  std_logic;
           grant2   : out std_logic);
	end component;

	signal cpu_cache_data		: std_logic_vector(31 downto 0);
	signal cpu_cache_address	: std_logic_vector(31 downto 0);
	signal cpu_cache_found		: std_logic;
	signal cpu_cache_find		: std_logic;
	signal cpu_cache_store		: std_logic;
	signal cpu_cache_load		: std_logic;
	signal cpu_cache_done		: std_logic;
	
	signal mem_cache_address	: std_logic_vector(31 downto 0);
	signal mem_cache_word		: std_logic_vector(31 downto 0);
	signal mem_cache_block		: std_logic_vector(127 downto 0);
	signal mem_cache_store		: std_logic;
	signal mem_cache_load		: std_logic;
	signal mem_cache_done		: std_logic;
	
	signal MC_cache_g				: std_logic;
	signal MC_cache_r				: std_logic;
	signal MC_cache_b				: std_logic;
	signal MC_mem_g				: std_logic;
	signal MC_mem_r				: std_logic;
	signal MC_mem_b				: std_logic;
	
	
	
begin
CPU_1: CPU port map(
				clk							=>	clk,
				reset							=> reset,
				bus_fromto_cpu_data		=> cpu_cache_data,
				bus_to_cpu_found			=> cpu_cache_found,
				bus_from_cpu_address		=> cpu_cache_address,
				bus_from_cpu_find			=> cpu_cache_find,
				cache_done					=> cpu_cache_done,
				cache_store					=> cpu_cache_store,
				cache_load					=> cpu_cache_load
);

CACHE_1: cache port map(
				clk							=> clk,
				BUS_address 				=> mem_cache_address,
				BUS_word  					=>	mem_cache_word,	
				BUS_block 					=> mem_cache_block,
				BUS_store 					=> mem_cache_store,
				BUS_load 					=> mem_cache_load,
				BUS_busy  					=> MC_cache_b,
				BUS_done   					=> mem_cache_done,
				MC_write_request  		=> MC_cache_r,
				MC_write_grant       	=> MC_cache_g,
				CPU_address          	=> cpu_cache_address,
				CPU_data   					=> cpu_cache_data,         
				CPU_find             	=> cpu_cache_find,
				CPU_found           		=> cpu_cache_found,
				CPU_store          		=> cpu_cache_store,
				CPU_load            		=> cpu_cache_load,
				CPU_done 					=> cpu_cache_done
);

MEMORY_1: Memory port map(
				clk          				=> clk,
				reset          			=> reset,
				bus_store          		=> mem_cache_store,
				bus_load          		=> mem_cache_load,
				bus_grant          		=> MC_mem_g,
				bus_address          	=> mem_cache_address,
				bus_word          		=> mem_cache_word,
				bus_block          		=> mem_cache_block,
				bus_done          		=> mem_cache_done,
				bus_req          			=> MC_mem_r,
				bus_busy						=> MC_mem_b
);

MAINCONTROLLER_1: mainController port map(
				clk          				=> clk,
				bus_busy1          		=> MC_cache_b,
				bus_busy2					=> MC_mem_b,
				request1          		=> MC_cache_r,
				grant1          			=> MC_cache_g,
				request2          		=> MC_mem_r,
				grant2          			=> MC_mem_g
);


					

end Behavioral;

