library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL; 

entity cacheI is
    generic ( WORD_SIZE  : integer :=32;
              BLOCK_SIZE : integer :=4 );
    port ( clk                  : in  std_logic;
           write_general_bus    : out std_logic:='0';
           bus_to_mem_address   : out std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
           bus_from_mem_word    : in  std_logic_vector (WORD_SIZE-1 downto 0);
           bus_from_mem_block   : in  std_logic_vector (WORD_SIZE*4-1 downto 0);
           bus_from_mem_found   : in  std_logic;
           bus_from_cpu_address : in  std_logic_vector (WORD_SIZE-1 downto 0);
           bus_to_cpu_data      : out std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
           bus_from_cpu_find    : in  std_logic;
           bus_to_cpu_found     : out std_logic:='0');
end cacheI;

architecture Behavioral of cacheI is
    --saved memory in cache
    type block_4words is array(0 to 3) of std_logic_vector(WORD_SIZE-1 downto 0); --type: block of 4 words (each with 32 bits)
    type col_32blocks is array(0 to 31) of block_4words;                 --type: column with 32 blocks (each with 4 words)
    type col_32tags is array(0 to 31) of std_logic_vector(4 downto 0);   --type: column with 32 tags (each with 5 bits)
    --per set (block)
    signal saved_valid_bit: std_logic_vector(31 downto 0)   --32 valid bits
    :=(5=>'1',others=>'0');  --32 valid bits
    signal saved_tag      : col_32tags                      --32 tags
    :=(5=>"10101",others=>"00000");
    signal saved_data     : col_32blocks                    --32 blocks with 4 words each
    :=(5=>(1=>x"AAAABBBB",others=>x"00000000"),others=>(others=>x"00000000")); --0th element of 5th element to x"0005", others to x"0000"
    signal data       : std_logic_vector (WORD_SIZE-1 downto 0);
    signal data_block : block_4words;
    
    --bus
    signal bus_address_tag   : std_logic_vector(4 downto 0);
    signal bus_address_index : std_logic_vector(4 downto 0);
    signal bus_address_offset: std_logic_vector(3 downto 0);
    --signal write :std_logic:='0';
    
    type state_type is (IDLE, FOUND, WAITING_FOR_MEM, DATA_RECEIVED_FROM_MEM);
    signal state : state_type :=IDLE;
    
    signal Rhit,Rmiss: std_logic:='0';
begin
    
    bus_address_tag    <= bus_from_cpu_address(13 downto 9);
    bus_address_index  <= bus_from_cpu_address(8 downto 4);
    bus_address_offset <= bus_from_cpu_address(3 downto 0);
    
    process(clk)
        variable value_index  :integer range 0 to WORD_SIZE-1;
        variable value_offset :integer range 0 to 3;
    begin
        if clk'event and clk='1' then
            if(state = IDLE and bus_from_cpu_find = '1') then
                value_index := to_integer(unsigned(bus_address_index));
                value_offset := to_integer(unsigned(bus_address_offset(3 downto 2)));
                if (saved_valid_bit(value_index) = '1') and (saved_tag(value_index) = bus_address_tag) then
                    data <= saved_data(value_index)(value_offset);
                    
                    state <= FOUND;
                    Rhit <= '1';
                else
                    state <= WAITING_FOR_MEM;
                    Rmiss <= '1';
                end if;
            
            elsif(state = WAITING_FOR_MEM and bus_from_mem_found = '1') then
                data_block(0) <= bus_from_mem_block(WORD_SIZE*4-1 downto WORD_SIZE*3);
                data_block(1) <= bus_from_mem_block(WORD_SIZE*3-1 downto WORD_SIZE*2);
                data_block(2) <= bus_from_mem_block(WORD_SIZE*2-1 downto WORD_SIZE);
                data_block(3) <= bus_from_mem_block(WORD_SIZE-1 downto 0);
                
                state <= DATA_RECEIVED_FROM_MEM;
            
            elsif(state = DATA_RECEIVED_FROM_MEM) then
                value_index := to_integer(unsigned(bus_address_index));
                value_offset := to_integer(unsigned(bus_address_offset(3 downto 2)));
                
                saved_valid_bit(value_index) <= '1';
                saved_tag(value_index) <= bus_address_tag;
                saved_data(value_index) <= data_block;
                data <= data_block(value_offset);
                
                state <= FOUND;
            
            elsif(state = FOUND and bus_from_cpu_find = '0') then
                state <= IDLE;
                Rhit <= '0';
                Rmiss <= '0';
            
            end if;
            
        end if;
    end process;
    
    process(state)
    begin
        case state is
            when FOUND =>
                bus_to_cpu_found <= '1';
                bus_to_cpu_data <= data;
                write_general_bus <= '0';
            when WAITING_FOR_MEM =>
                bus_to_mem_address <= bus_from_cpu_address;
                write_general_bus <= '1';
            when DATA_RECEIVED_FROM_MEM =>
                write_general_bus <= '0';
            when others =>
                bus_to_cpu_found <= '0';
                write_general_bus <= '0';
                bus_to_cpu_data <= (others=>'Z');
        end case;
    end process;
    
end Behavioral;
