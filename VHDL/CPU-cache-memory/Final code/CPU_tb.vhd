LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY CPU_tb IS
END CPU_tb;
 
ARCHITECTURE behavior OF CPU_tb IS 
 
 
    COMPONENT CPU
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         bus_fromto_cpu_data : INOUT  std_logic_vector(31 downto 0)	:= (others => '0');
         bus_to_cpu_found : IN  std_logic;
         bus_from_cpu_address : OUT  std_logic_vector(31 downto 0);
         bus_from_cpu_find : OUT  std_logic;
			cache_done : IN std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal bus_to_cpu_found : std_logic := '0';
	signal cache_done : std_logic := '0';

	--BiDirs
   signal bus_fromto_cpu_data : std_logic_vector(31 downto 0) := (others => '0');

 	--Outputs
   signal bus_from_cpu_address : std_logic_vector(31 downto 0);
   signal bus_from_cpu_find : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CPU PORT MAP (
          clk => clk,
          reset => reset,
          bus_fromto_cpu_data => bus_fromto_cpu_data,
          bus_to_cpu_found => bus_to_cpu_found,
          bus_from_cpu_address => bus_from_cpu_address,
          bus_from_cpu_find => bus_from_cpu_find,
			 cache_done => cache_done
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		
			
		reset <= '0';
		wait for 10 ns;
		reset <= '1';
		
		wait until bus_from_cpu_find = '1';
		bus_fromto_cpu_data <= "10001100000001110000001111111111"; --LW R7,1023(R0)
		wait for 5 ns;
		bus_to_cpu_found <= '1';
		cache_done <= '1';
		wait for 20 ns;
		bus_to_cpu_found <= '0';
		cache_done <= '0';
		
		wait until bus_from_cpu_find= '1';
		bus_fromto_cpu_data <= "00000000111111110000000011111111"; --16711935
		wait for 5 ns;
		bus_to_cpu_found <= '1';
		wait for 20 ns;
		bus_to_cpu_found <= '0';
		
		wait until bus_from_cpu_find = '1';
		bus_fromto_cpu_data <= "10001100000001010000000000000000"; --LW R5,0(R0)
		wait for 5 ns;
		bus_to_cpu_found <= '1';
		wait for 20 ns;
		bus_to_cpu_found <= '0';
		
		
		wait until bus_from_cpu_find= '1';
		bus_fromto_cpu_data <= "00000000000000000000000000000001"; --1
		wait for 5 ns;
		bus_to_cpu_found <= '1';
		wait for 20 ns;
		bus_to_cpu_found <= '0';
		
		wait until bus_from_cpu_find= '1';
		--bus_fromto_cpu_data <= "00000000101001110001100000100000"; --ADD R3,R5,R7
		--bus_fromto_cpu_data <= "00000000101001110001100000100111"; --NOR R3,R5,R7
		bus_fromto_cpu_data <= "00000000101000000010101111000000"; --SLL R5,R5,15
		wait for 5 ns;
		bus_to_cpu_found <= '1';
		wait for 20 ns;
		bus_to_cpu_found <= '0';
		
		wait until bus_from_cpu_find= '1';
		bus_fromto_cpu_data <= "00000000101001110001100000100000"; --ADD R3,R5,R7
		--bus_fromto_cpu_data <= "00000000101001110001100000100111"; --NOR R3,R5,R7
		--bus_fromto_cpu_data <= "00000000101000000001101111000000"; --SLL R3,R5,15
		wait for 5 ns;
		bus_to_cpu_found <= '1';
		wait for 20 ns;
		bus_to_cpu_found <= '0';
		
		
		
      	
		

      wait;
   end process;

END;
