

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mainController_tb is
--  Port ( );
end mainController_tb;

architecture Behavioral of mainController_tb is
    component mainController is
        port ( clk      : in  std_logic;
               bus_busy1: in  std_logic;
               bus_busy2: in  std_logic;
               request1 : in  std_logic;
               grant1   : out std_logic;
               request2 : in  std_logic;
               grant2   : out std_logic);
    end component;
    signal clk      : std_logic;
    signal bus_busy1: std_logic;
    signal bus_busy2: std_logic;
    signal request1 : std_logic;
    signal grant1   : std_logic;
    signal request2 : std_logic;
    signal grant2   : std_logic;
    
    constant clk_period : time := 1 ns;
begin
MC: mainController port map(clk,
                            bus_busy1,
                            bus_busy2,
                            request1,
                            grant1,
                            request2,
                            grant2);
    
    process
    begin
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
        wait for clk_period/2;
    end process;
    
    process
    begin
        request1 <= '1';
        request2 <= '0';
        
        bus_busy1 <= '1';
        bus_busy2 <= '0';
        wait for 2 ns;
        bus_busy1 <= '0';
        bus_busy2 <= '1';
        wait for 2 ns;
        
        request1 <= '1';
        request2 <= '1';
        wait for 2 ns;
        request1 <= '0';
        request2 <= '1';
        wait for 2 ns;
        request1 <= '0';
        request2 <= '0';
        wait for 2 ns;
        
        bus_busy1 <= '0';
        bus_busy2 <= '0';
                
        request1 <= '0';
        request2 <= '0';
        wait for 2 ns;
        request1 <= '1';
        request2 <= '1';
        wait for 1 ns;
            if(grant1 = '1' or grant2 ='1') then
                bus_busy1 <= '1';
                wait for 3 ns;
                bus_busy1 <= '0';
            end if;
        request1 <= '0';
        request2 <= '1';
        wait for 1 ns;
            if(grant1 = '1' or grant2 ='1') then
                bus_busy1 <= '1';
                wait for 3 ns;
                bus_busy1 <= '0';
            end if;
        request1 <= '0';
        request2 <= '0';
        wait for 2 ns;
        
    end process;

end Behavioral;
