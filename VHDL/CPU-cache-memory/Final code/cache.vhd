library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cache is
    generic ( WORD_SIZE  : integer :=32;
              BLOCK_SIZE : integer :=4 );
    port ( clk                   : in    std_logic;
           BUS_address           : out   std_logic_vector (WORD_SIZE-1 downto 0);
           BUS_word              : inout std_logic_vector (WORD_SIZE-1 downto 0);
           BUS_block             : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
           BUS_store             : out   std_logic;
           BUS_load              : out   std_logic;
           BUS_busy              : inout std_logic;
           BUS_done              : in    std_logic;
           MC_write_request      : out   std_logic;
           MC_write_grant        : in    std_logic;
           CPU_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);
           CPU_data              : inout std_logic_vector (WORD_SIZE-1 downto 0);
           CPU_find              : in    std_logic;
           CPU_found             : out   std_logic;
           CPU_store             : in    std_logic;
           CPU_load              : in    std_logic;
           CPU_done              : out   std_logic);
end cache;

architecture Behavioral of cache is
    
    component cacheI is
        port ( clk                  : in  std_logic;
               write_general_bus    : out std_logic;
               bus_to_mem_address   : out std_logic_vector (WORD_SIZE-1 downto 0);
               bus_from_mem_word    : in  std_logic_vector (WORD_SIZE-1 downto 0);
               bus_from_mem_block   : in  std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               bus_from_mem_found   : in  std_logic;
               bus_from_cpu_address : in  std_logic_vector (WORD_SIZE-1 downto 0);
               bus_to_cpu_data      : out std_logic_vector (WORD_SIZE-1 downto 0);
               bus_from_cpu_find    : in  std_logic;
               bus_to_cpu_found     : out std_logic);
    end component;
    
    component cacheD is
        port ( clk                   : in    std_logic;
               write_general_bus     : out   std_logic;
               bus_to_mem_address    : out   std_logic_vector (WORD_SIZE-1 downto 0);
               bus_fromto_mem_word   : inout std_logic_vector (WORD_SIZE-1 downto 0);
               bus_fromto_mem_block  : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               bus_to_mem_store      : out   std_logic;
               bus_to_mem_load       : out   std_logic;
               bus_from_mem_done     : in    std_logic;
               bus_from_cpu_Daddress : in    std_logic_vector (WORD_SIZE-1 downto 0);
               bus_fromto_cpu_data   : inout std_logic_vector (WORD_SIZE-1 downto 0);
               bus_from_cpu_store    : in    std_logic;
               bus_from_cpu_load     : in    std_logic;
               bus_to_cpu_done       : out   std_logic);
    end component;
    
    component cacheController is
        port ( clk                  : in    std_logic;
               CI_write_request     : in    std_logic;                                        --CI = cache I
               CI_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);          --CD = cache D
               CI_word              : out   std_logic_vector (WORD_SIZE-1 downto 0);          --MC = main controller
               CI_block             : out   std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               CI_found             : out   std_logic;
               CD_write_request     : in    std_logic;
               CD_address           : in    std_logic_vector (WORD_SIZE-1 downto 0);
               CD_word              : inout std_logic_vector (WORD_SIZE-1 downto 0);
               CD_block             : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               CD_store             : in    std_logic;
               CD_load              : in    std_logic;
               CD_done              : out   std_logic;
               BUS_address          : out   std_logic_vector (WORD_SIZE-1 downto 0);
               BUS_word             : inout std_logic_vector (WORD_SIZE-1 downto 0);
               BUS_block            : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
               BUS_store            : out   std_logic;
               BUS_load             : out   std_logic;
               BUS_busy             : inout std_logic;
               BUS_done             : in    std_logic;
               MC_write_request     : out   std_logic;
               MC_write_grant       : in    std_logic);
    end component;
    signal CI_write_request    : std_logic;                                          --CI = cache I
    signal CI_address          : std_logic_vector (WORD_SIZE-1 downto 0);            --CD = cache D
    signal CI_word             : std_logic_vector (WORD_SIZE-1 downto 0);            --MC = main controller
    signal CI_block            : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal CI_found            : std_logic;
    signal CD_write_request    : std_logic;
    signal CD_address          : std_logic_vector (WORD_SIZE-1 downto 0);
    signal CD_word             : std_logic_vector (WORD_SIZE-1 downto 0);
    signal CD_block            : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal CD_store            : std_logic;
    signal CD_load             : std_logic;
    signal CD_done             : std_logic;
    
begin

Cache_I: cacheI port map(clk        => clk,
               write_general_bus    => CI_write_request,
               bus_to_mem_address   => CI_address,
               bus_from_mem_word    => CI_word,
               bus_from_mem_block   => CI_block,
               bus_from_mem_found   => CI_found,
               bus_from_cpu_address => CPU_address,
               bus_to_cpu_data      => CPU_data,
               bus_from_cpu_find    => CPU_find,
               bus_to_cpu_found     => CPU_found);

Cache_D: cacheD port map(clk         => clk,
               write_general_bus     => CD_write_request,
               bus_to_mem_address    => CD_address,
               bus_fromto_mem_word   => CD_word,
               bus_fromto_mem_block  => CD_block,
               bus_to_mem_store      => CD_store,
               bus_to_mem_load       => CD_load,
               bus_from_mem_done     => CD_done,
               bus_from_cpu_Daddress => CPU_address,
               bus_fromto_cpu_data   => CPU_data,
               bus_from_cpu_store    => CPU_store,
               bus_from_cpu_load     => CPU_load,
               bus_to_cpu_done       => CPU_done);

Cache_Controller: cacheController port map(clk      => clk,
                                   CI_write_request => CI_write_request,
                                   CI_address       => CI_address,
                                   CI_word          => CI_word,
                                   CI_block         => CI_block,
                                   CI_found         => CI_found,
                                   CD_write_request => CD_write_request,
                                   CD_address       => CD_address,
                                   CD_word          => CD_word,
                                   CD_block         => CD_block,
                                   CD_store         => CD_store,
                                   CD_load          => CD_load,
                                   CD_done          => CD_done,
                                   BUS_address      => BUS_address,
                                   BUS_word         => BUS_word,
                                   BUS_block        => BUS_block,
                                   BUS_store        => BUS_store,
                                   BUS_load         => BUS_load,
                                   BUS_busy         => BUS_busy,
                                   BUS_done         => BUS_done,
                                   MC_write_request => MC_write_request,
                                   MC_write_grant   => MC_write_grant);



end Behavioral;
