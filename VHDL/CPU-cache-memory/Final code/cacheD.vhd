library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL; 

entity cacheD is
    generic ( WORD_SIZE  : integer :=32;
              BLOCK_SIZE : integer :=4 );
    port ( clk                   : in    std_logic;
           write_general_bus     : out   std_logic:='0';
           bus_to_mem_address    : out   std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
           bus_fromto_mem_word   : inout std_logic_vector (WORD_SIZE-1 downto 0);
           bus_fromto_mem_block  : inout std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
           bus_to_mem_store      : out   std_logic:='0';
           bus_to_mem_load       : out   std_logic:='0';
           bus_from_mem_done     : in    std_logic;
           bus_from_cpu_Daddress : in    std_logic_vector (WORD_SIZE-1 downto 0);
           bus_fromto_cpu_data   : inout std_logic_vector (WORD_SIZE-1 downto 0);
           bus_from_cpu_store    : in    std_logic;
           bus_from_cpu_load     : in    std_logic;
           bus_to_cpu_done       : out   std_logic:='0');
end cacheD;

architecture Behavioral of cacheD is
    --saved memory in cache
    type block_4words is array(0 to 3) of std_logic_vector(WORD_SIZE-1 downto 0); --type: block of 4 words (each with 32 bits)
    type col_16blocks is array(0 to 15) of block_4words;                 --type: column with 16 blocks (each with 4 words)
    type col_16tags is array(0 to 15) of std_logic_vector(5 downto 0);   --type: column with 16 tags (each with 6 bits)
    --per set (block)
    signal saved_valid_bit: std_logic_vector(15 downto 0)   --16 valid bits
    :=(5=>'1',others=>'0');  --32 valid bits
    signal saved_tag      : col_16tags                      --16 tags
    :=(5=>"010101",others=>"000000");
    signal saved_data     : col_16blocks                    --16 blocks with 4 words each
    :=(5=>(1=>x"AAAABBBB",others=>x"00000000"),others=>(others=>x"00000000")); --0th element of 5th element to x"0005", others to x"0000"
    signal data       : std_logic_vector (WORD_SIZE-1 downto 0);
    signal data_block : block_4words;
    
    --bus
    signal bus_address_tag   : std_logic_vector(5 downto 0);
    signal bus_address_index : std_logic_vector(3 downto 0);
    signal bus_address_offset: std_logic_vector(3 downto 0);
    signal write :std_logic:='0';
    
    type state_type is (IDLE, FOUND,   WAITING_FOR_MEM_READ,  DATA_RECEIVED_FROM_MEM,
                              WRITTEN, WAITING_FOR_MEM_WRITE, DATA_WRITTEN_IN_MEM);
    signal state : state_type :=IDLE;
    
    --signals to control inout (bidirectional) ports
    signal bus_fromto_mem_word_Write  : std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
    signal bus_fromto_mem_word_Read   : std_logic_vector (WORD_SIZE-1 downto 0);
    signal en_bus_fromto_mem_word     : std_logic :='1';
    
    signal bus_fromto_mem_block_Write  : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0):=(others=>'0');
    signal bus_fromto_mem_block_Read   : std_logic_vector (WORD_SIZE*BLOCK_SIZE-1 downto 0);
    signal en_bus_fromto_mem_block     : std_logic :='1';
    
    signal bus_fromto_cpu_data_Write  : std_logic_vector (WORD_SIZE-1 downto 0):=(others=>'0');
    signal bus_fromto_cpu_data_Read   : std_logic_vector (WORD_SIZE-1 downto 0);
    signal en_bus_fromto_cpu_data     : std_logic :='1';
    
    signal Rhit,Rmiss,Whit,Wmiss: std_logic:='0';
begin
    
    bus_address_tag    <= bus_from_cpu_Daddress(13 downto 8);
    bus_address_index  <= bus_from_cpu_Daddress(7 downto 4);
    bus_address_offset <= bus_from_cpu_Daddress(3 downto 0);
    
    process(clk)
        variable value_index  :integer range 0 to 15;
        variable value_offset :integer range 0 to 3;
    begin
        if clk'event and clk='1' then
        
            --states for reading
            
            if(state = IDLE and bus_from_cpu_load = '1') then
                value_index := to_integer(unsigned(bus_address_index));
                value_offset := to_integer(unsigned(bus_address_offset(3 downto 2)));
                if (saved_valid_bit(value_index) = '1') and (saved_tag(value_index) = bus_address_tag) then
                    data <= saved_data(value_index)(value_offset);
                    
                    state <= FOUND;
                    Rhit <= '1';
                    en_bus_fromto_cpu_data <= '0';
                else
                    state <= WAITING_FOR_MEM_READ;
                    Rmiss <= '1';
                    en_bus_fromto_mem_block <= '1';
                end if;
            
            elsif(state = WAITING_FOR_MEM_READ and bus_from_mem_done = '1') then
                data_block(3) <= bus_fromto_mem_block_Read(WORD_SIZE*4-1 downto WORD_SIZE*3);
                data_block(2) <= bus_fromto_mem_block_Read(WORD_SIZE*3-1 downto WORD_SIZE*2);
                data_block(1) <= bus_fromto_mem_block_Read(WORD_SIZE*2-1 downto WORD_SIZE);
                data_block(0) <= bus_fromto_mem_block_Read(WORD_SIZE-1 downto 0);
                
                state <= DATA_RECEIVED_FROM_MEM;
            
            elsif(state = DATA_RECEIVED_FROM_MEM) then
                value_index := to_integer(unsigned(bus_address_index));
                value_offset := to_integer(unsigned(bus_address_offset(3 downto 2)));
                
                saved_valid_bit(value_index) <= '1';
                saved_tag(value_index) <= bus_address_tag;
                saved_data(value_index) <= data_block;
                data <= data_block(value_offset);
                
                en_bus_fromto_cpu_data <= '0';
                state <= FOUND;
            
            elsif(state = FOUND and bus_from_cpu_load = '0') then
                state <= IDLE;
                Rhit <= '0';
                Rmiss <= '0';
                en_bus_fromto_cpu_data <= '1';
                en_bus_fromto_mem_word <= '0';
            
            --states for writing
            
            elsif(state = IDLE and bus_from_cpu_store = '1') then
                value_index := to_integer(unsigned(bus_address_index));
                value_offset := to_integer(unsigned(bus_address_offset(3 downto 2)));
                if (saved_valid_bit(value_index) = '1') and (saved_tag(value_index) = bus_address_tag) then
                    saved_data(value_index)(value_offset) <= bus_fromto_cpu_data_Read;
                    Whit <= '1';
                else
                    Wmiss <= '1';
                end if;
                state <= WAITING_FOR_MEM_WRITE; --write memory whether hits or misses (write thru)
                bus_fromto_mem_word_Write <= bus_fromto_cpu_data_Read;
            
            elsif(state = WAITING_FOR_MEM_WRITE and bus_from_mem_done = '1') then
                
                state <= DATA_WRITTEN_IN_MEM;
            
            elsif(state = DATA_WRITTEN_IN_MEM and bus_from_cpu_store = '0') then
                
                state <= IDLE;
                Whit <= '0';
                Wmiss <= '0';
                en_bus_fromto_cpu_data <= '1';
                en_bus_fromto_mem_word <= '0';
            
            end if;
            
        end if;

    end process;
    
    process(state)
    begin
        case state is
            --states for reading
            when IDLE =>
                bus_to_cpu_done <= '0';
                write_general_bus <= '0';
            when FOUND =>
                bus_to_cpu_done <= '1';
                bus_fromto_cpu_data_Write <= data;
                write_general_bus <= '0';
                en_bus_fromto_mem_block <= '1';
            when WAITING_FOR_MEM_READ =>
                bus_to_mem_address <= bus_from_cpu_Daddress;
                bus_to_mem_load <= bus_from_cpu_load;
                write_general_bus <= '1';
            when DATA_RECEIVED_FROM_MEM =>
                bus_to_mem_load <= '0';
                write_general_bus <= '0';
            --states for writing
            when WAITING_FOR_MEM_WRITE =>
                bus_to_mem_address <= bus_from_cpu_Daddress;
                bus_to_mem_store <= bus_from_cpu_store;
                write_general_bus <= '1';
            when DATA_WRITTEN_IN_MEM =>
                bus_to_mem_store <= '0';
                bus_to_cpu_done <= '1';
                write_general_bus <= '0';
            --rest of states
            when others =>
                bus_to_cpu_done <= '0';
                write_general_bus <= '0';
        end case;
    end process;
    
    bus_fromto_mem_block <= (others=>'Z') when en_bus_fromto_mem_block = '1'  --set enable to be able to read inout port
                 else bus_fromto_mem_block_Write;
    bus_fromto_mem_block_Read <= bus_fromto_mem_block;
    
    bus_fromto_mem_word <= (others=>'Z') when en_bus_fromto_mem_word = '1'  --set enable to be able to read inout port
                 else bus_fromto_mem_word_Write;
    bus_fromto_mem_word_Read <= bus_fromto_mem_word;
    
    bus_fromto_cpu_data <= (others=>'Z') when en_bus_fromto_cpu_data = '1'  --set enable to be able to read inout port
                 else bus_fromto_cpu_data_Write;
    bus_fromto_cpu_data_Read <= bus_fromto_cpu_data;
    
end Behavioral;
