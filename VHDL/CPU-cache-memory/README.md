#CPU-cache-memory project

Project done by Pablo Menendez Blanco and Carlos Santos Rancano for ECE585 Advanced Computer Architecture at IIT.

**Work distribution**

Module  | Author
------------- | -------------
CPU  | Carlos Santos Rancano
cacheI | Pablo Menendez Blanco 
cacheD | Pablo Menendez Blanco
cacheController | Pablo Menendez Blanco
memory | Carlos Santos Rancano
mainController | Pablo Menendez Blanco
TOP_MODULE | Carlos Santos Rancano

