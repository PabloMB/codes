library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity arithmetic_barrel_shifter_tb is

end arithmetic_barrel_shifter_tb;

architecture Behavioral of arithmetic_barrel_shifter_tb is
	component arithmetic_barrel_shifter is
		port ( A   : in std_logic_vector(7 downto 0);
			   S   : in std_logic_vector(2 downto 0);
			   DIR : in std_logic;
			   O   : out std_logic_vector(7 downto 0));
	end component;
signal A   : std_logic_vector(7 downto 0);
signal S   : std_logic_vector(2 downto 0);
signal DIR : std_logic;
signal O   : std_logic_vector(7 downto 0);
begin
abs1: arithmetic_barrel_shifter port map(A,S,DIR,O);
	
	process
    begin
		A <= "XXXXXXXX";
        S <= "XXX";
        DIR <= 'X';
		wait for 1 ns;
		
		DIR <= '0';
        
		A <= "00000001";
        S <= "000";
        wait for 1 ns;
		
		for I in 0 to 6 loop
			S <= std_logic_vector(to_unsigned(to_integer(unsigned( S )) + 1, 3));
			wait for 1 ns;
		end loop;
		
        DIR <= '1';
		wait for 1 ns;
		
		A <= "10000000";
        S <= "001";
		wait for 1 ns;
		
		for I in 0 to 6 loop
			S <= std_logic_vector(to_unsigned(to_integer(unsigned( S )) + 1, 3));
			wait for 1 ns;
		end loop;
		
		
 end process;

end Behavioral;

