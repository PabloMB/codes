library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity arithmetic_barrel_shifter is
    port ( A   : in std_logic_vector(7 downto 0);
           S   : in std_logic_vector(2 downto 0);
		   DIR : in std_logic;
           O   : out std_logic_vector(7 downto 0));
end arithmetic_barrel_shifter;

architecture Behavioral of arithmetic_barrel_shifter is
	component mux2to1 is
		port ( A   : in std_logic;
			   B   : in std_logic;
			   sel : in std_logic;
			   X   : out std_logic);
	end component;
signal sL1: std_logic_vector(7 downto 0);
signal sL2: std_logic_vector(7 downto 0);
signal sL3: std_logic_vector(7 downto 0);
signal sR1: std_logic_vector(7 downto 0);
signal sR2: std_logic_vector(7 downto 0);
signal sR3: std_logic_vector(7 downto 0);
begin

--shift left
mL1_0: mux2to1 port map(A(0),'0',S(0),sL1(0));
mL1_1: mux2to1 port map(A(1),A(0),S(0),sL1(1));
mL1_2: mux2to1 port map(A(2),A(1),S(0),sL1(2));
mL1_3: mux2to1 port map(A(3),A(2),S(0),sL1(3));
mL1_4: mux2to1 port map(A(4),A(3),S(0),sL1(4));
mL1_5: mux2to1 port map(A(5),A(4),S(0),sL1(5));
mL1_6: mux2to1 port map(A(6),A(5),S(0),sL1(6));
mL1_7: mux2to1 port map(A(7),A(6),S(0),sL1(7));
	
mL2_0: mux2to1 port map(sL1(0),'0',S(1),sL2(0));
mL2_1: mux2to1 port map(sL1(1),'0',S(1),sL2(1));
mL2_2: mux2to1 port map(sL1(2),sL1(0),S(1),sL2(2));
mL2_3: mux2to1 port map(sL1(3),sL1(1),S(1),sL2(3));
mL2_4: mux2to1 port map(sL1(4),sL1(2),S(1),sL2(4));
mL2_5: mux2to1 port map(sL1(5),sL1(3),S(1),sL2(5));
mL2_6: mux2to1 port map(sL1(6),sL1(4),S(1),sL2(6));
mL2_7: mux2to1 port map(sL1(7),sL1(5),S(1),sL2(7));

mL3_0: mux2to1 port map(sL2(0),'0',S(2),sL3(0));
mL3_1: mux2to1 port map(sL2(1),'0',S(2),sL3(1));
mL3_2: mux2to1 port map(sL2(2),'0',S(2),sL3(2));
mL3_3: mux2to1 port map(sL2(3),'0',S(2),sL3(3));
mL3_4: mux2to1 port map(sL2(4),sL2(0),S(2),sL3(4));
mL3_5: mux2to1 port map(sL2(5),sL2(1),S(2),sL3(5));
mL3_6: mux2to1 port map(sL2(6),sL2(2),S(2),sL3(6));
mL3_7: mux2to1 port map(sL2(7),sL2(3),S(2),sL3(7));

--shift right
mR1_0: mux2to1 port map(A(0),A(1),S(0),sR1(0));
mR1_1: mux2to1 port map(A(1),A(2),S(0),sR1(1));
mR1_2: mux2to1 port map(A(2),A(3),S(0),sR1(2));
mR1_3: mux2to1 port map(A(3),A(4),S(0),sR1(3));
mR1_4: mux2to1 port map(A(4),A(5),S(0),sR1(4));
mR1_5: mux2to1 port map(A(5),A(6),S(0),sR1(5));
mR1_6: mux2to1 port map(A(6),A(7),S(0),sR1(6));
mR1_7: mux2to1 port map(A(7),A(7),S(0),sR1(7));

mR2_0: mux2to1 port map(sR1(0),sR1(2),S(1),sR2(0));
mR2_1: mux2to1 port map(sR1(1),sR1(3),S(1),sR2(1));
mR2_2: mux2to1 port map(sR1(2),sR1(4),S(1),sR2(2));
mR2_3: mux2to1 port map(sR1(3),sR1(5),S(1),sR2(3));
mR2_4: mux2to1 port map(sR1(4),sR1(6),S(1),sR2(4));
mR2_5: mux2to1 port map(sR1(5),sR1(7),S(1),sR2(5));
mR2_6: mux2to1 port map(sR1(6),sR1(7),S(1),sR2(6));
mR2_7: mux2to1 port map(sR1(7),sR1(7),S(1),sR2(7));

mR3_0: mux2to1 port map(sR2(0),sR2(4),S(2),sR3(0));
mR3_1: mux2to1 port map(sR2(1),sR2(5),S(2),sR3(1));
mR3_2: mux2to1 port map(sR2(2),sR2(6),S(2),sR3(2));
mR3_3: mux2to1 port map(sR2(3),sR2(7),S(2),sR3(3));
mR3_4: mux2to1 port map(sR2(4),sR2(7),S(2),sR3(4));
mR3_5: mux2to1 port map(sR2(5),sR2(7),S(2),sR3(5));
mR3_6: mux2to1 port map(sR2(6),sR2(7),S(2),sR3(6));
mR3_7: mux2to1 port map(sR2(7),sR2(7),S(2),sR3(7));

--choosing shift
mD1_0: mux2to1 port map(sL3(0),sR3(0),DIR,O(0));
mD1_1: mux2to1 port map(sL3(1),sR3(1),DIR,O(1));
mD1_2: mux2to1 port map(sL3(2),sR3(2),DIR,O(2));
mD1_3: mux2to1 port map(sL3(3),sR3(3),DIR,O(3));
mD1_4: mux2to1 port map(sL3(4),sR3(4),DIR,O(4));
mD1_5: mux2to1 port map(sL3(5),sR3(5),DIR,O(5));
mD1_6: mux2to1 port map(sL3(6),sR3(6),DIR,O(6));
mD1_7: mux2to1 port map(sL3(7),sR3(7),DIR,O(7));
	
end Behavioral;
