library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux2to1 is
    port ( A   : in std_logic;
           B   : in std_logic;
		   sel : in std_logic;
           X   : out std_logic);
end mux2to1;

architecture Behavioral of mux2to1 is
begin

	x <= A when sel='0' else B;
	
end Behavioral;