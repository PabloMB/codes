library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity traffic_light_controller_tb is

end traffic_light_controller_tb;

architecture Behavioral of traffic_light_controller_tb is
    component traffic_light_controller is
        port ( CLK : in std_logic;
               NS_R : out std_logic;
               NS_Y : out std_logic;
               NS_G : out std_logic;
               EW_R : out std_logic;
               EW_Y : out std_logic;
               EW_G : out std_logic);
    end component;
signal NS_R,NS_Y,NS_G,EW_R,EW_Y,EW_G: std_logic;
signal CLK: std_logic:='0';
constant CLK_PERIOD: time := 1 ns;
begin
tlc: traffic_light_controller port map(CLK,NS_R,NS_Y,NS_G,EW_R,EW_Y,EW_G);
    
    process
    begin
        CLK <= '0';
        wait for CLK_PERIOD/2;
        CLK <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    process
    begin
        wait for 100 ns;
    end process;
    
end Behavioral;
