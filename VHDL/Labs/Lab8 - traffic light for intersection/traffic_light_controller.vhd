library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity traffic_light_controller is
    port ( CLK : in std_logic;
           NS_R : out std_logic;
           NS_Y : out std_logic;
           NS_G : out std_logic;
           EW_R : out std_logic;
           EW_Y : out std_logic;
           EW_G : out std_logic);
end traffic_light_controller;

architecture Behavioral of traffic_light_controller is
type state_type is (GR,YR,RR1,RG,RY,RR2);
signal state : state_type :=GR;
signal count: std_logic_vector(3 downto 0):="0001";
begin
    
    process(CLK,state,count)
    begin
        if CLK'event and CLK='1' then
               count <= std_logic_vector(to_unsigned(to_integer(unsigned(count)) + 1,4));
            if state=GR then
               if count="1010" then
                   count <= "0001";
                   state <= YR;
               end if;
           elsif state=YR then
               if count="0001" then
                   count <= "0001";
                   state <= RR1;
               end if;
           elsif state=RR1 then
               if count="0001" then
                   count <= "0001";
                   state <= RG;
               end if;
           elsif state=RG then
               if count="0101" then
                   count <= "0001";
                   state <= RY;
               end if;
           elsif state=RY then
               if count="0001" then
                   count <= "0001";
                   state <= RR2;
               end if;
           elsif state=RR2 then
               if count="0001" then
                   count <= "0001";
                   state <= GR;
               end if;
           end if;
        end if;
    end process;

    process(state)
    begin
        if state=GR then
            NS_G <= '1';
            NS_Y <= '0';
            NS_R <= '0';
            EW_G <= '0';
            EW_Y <= '0';
            EW_R <= '1';
        elsif state=YR then
            NS_G <= '0';
            NS_Y <= '1';
            NS_R <= '0';
            EW_G <= '0';
            EW_Y <= '0';
            EW_R <= '1';
        elsif state=RR1 then
            NS_G <= '0';
            NS_Y <= '0';
            NS_R <= '1';
            EW_G <= '0';
            EW_Y <= '0';
            EW_R <= '1';
        elsif state=RG then
            NS_G <= '0';
            NS_Y <= '0';
            NS_R <= '1';
            EW_G <= '1';
            EW_Y <= '0';
            EW_R <= '0';
        elsif state=RY then
            NS_G <= '0';
            NS_Y <= '0';
            NS_R <= '1';
            EW_G <= '0';
            EW_Y <= '1';
            EW_R <= '0';
        elsif state=RR2 then
            NS_G <= '0';
            NS_Y <= '0';
            NS_R <= '1';
            EW_G <= '0';
            EW_Y <= '0';
            EW_R <= '1';
        end if;
    end process;    

end Behavioral;
