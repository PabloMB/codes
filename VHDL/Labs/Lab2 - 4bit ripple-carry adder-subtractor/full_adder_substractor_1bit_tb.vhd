library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity full_adder_substractor_1bit_tb is

end full_adder_substractor_1bit_tb;

architecture Behavioral of full_adder_substractor_1bit_tb is
	component full_adder_substractor_1bit is
		port ( a      : in std_logic;
			   b      : in std_logic;
			   ci     : in std_logic;
			   op_sel : in std_logic;
			   s      : out std_logic;
			   co     : out std_logic);
	end component;
	signal a,b,op_sel,s,co: std_logic;
begin
fas1b: full_adder_substractor_1bit port map(a,b,op_sel,op_sel,s,co);
	
	process
    begin
		a <= 'X';
        b <= 'X';
        op_sel <= 'X';
		wait for 1 ns;
		
		op_sel <= '0';
		
		a <= '0';
        b <= '0';
        wait for 1 ns;
		
		a <= '0';
        b <= '1';
        wait for 1 ns;
		
		a <= '1';
        b <= '1';
        wait for 1 ns;
		
		a <= '1';
        b <= '0';
        wait for 1 ns;
		
		op_sel <= '1';
		wait for 1 ns;
				
		a <= '0';
        b <= '0';
        wait for 1 ns;
		
		a <= '0';
        b <= '1';
        wait for 1 ns;
		
		a <= '1';
        b <= '1';
        wait for 1 ns;
		
		a <= '1';
        b <= '0';
        wait for 1 ns;
		
 end process;

end Behavioral;
