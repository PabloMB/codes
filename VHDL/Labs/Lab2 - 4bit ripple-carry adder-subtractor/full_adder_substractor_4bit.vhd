library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity full_adder_substractor_4bit is
    port ( A      : in std_logic_vector(3 downto 0);
           B      : in std_logic_vector(3 downto 0);
           CI     : in std_logic;
		   OP_SEL : in std_logic;
           S      : out std_logic_vector(3 downto 0);
           CO     : out std_logic);
end full_adder_substractor_4bit;

architecture Behavioral of full_adder_substractor_4bit is
	component full_adder_substractor_1bit is
		port ( a      : in std_logic;
			   b      : in std_logic;
			   ci     : in std_logic;
			   op_sel : in std_logic;
			   s      : out std_logic;
			   co     : out std_logic);
	end component;
	signal c_internal: std_logic_vector(2 downto 0);
begin
	
fas0: full_adder_substractor_1bit port map(A(0),B(0),CI           ,OP_SEL,S(0),c_internal(0));
fas1: full_adder_substractor_1bit port map(A(1),B(1),c_internal(0),OP_SEL,S(1),c_internal(1));
fas2: full_adder_substractor_1bit port map(A(2),B(2),c_internal(1),OP_SEL,S(2),c_internal(2));
fas3: full_adder_substractor_1bit port map(A(3),B(3),c_internal(2),OP_SEL,S(3),CO);
	
end Behavioral;
