library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity full_adder_substractor_4bit_tb is

end full_adder_substractor_4bit_tb;

architecture Behavioral of full_adder_substractor_4bit_tb is
	component full_adder_substractor_4bit is
		port ( A      : in std_logic_vector(3 downto 0);
               B      : in std_logic_vector(3 downto 0);
               CI     : in std_logic;
               OP_SEL : in std_logic;
               S      : out std_logic_vector(3 downto 0);
               CO     : out std_logic);
	end component;
	signal A,B,S: std_logic_vector(3 downto 0);
	signal OP_SEL,CO: std_logic;
begin
fas4b: full_adder_substractor_4bit port map(A,B,OP_SEL,OP_SEL,S,CO);
	
	process
    begin
		A <= "XXXX";
        B <= "XXXX";
        OP_SEL <= 'X';
		wait for 1 ns;
		
		OP_SEL <= '0';
		
		A <= "0000";
        B <= "0000";
		wait for 1 ns;
		
		A <= "1010";
        B <= "0101";
		wait for 1 ns;
		
		A <= "1001";
        B <= "0111";
		wait for 1 ns;
		
		A <= "1111";
        B <= "0001";
		wait for 1 ns;
		
		OP_SEL <= '1';
		wait for 1 ns;
        
		A <= "0000";
        B <= "0000";
		wait for 1 ns;
		
		A <= "1010";
        B <= "0101";
		wait for 1 ns;
		
		A <= "0101";
        B <= "1010";
		wait for 1 ns;
		
		A <= "1111";
        B <= "0001";
		wait for 1 ns;
		
		
 end process;

end Behavioral;
