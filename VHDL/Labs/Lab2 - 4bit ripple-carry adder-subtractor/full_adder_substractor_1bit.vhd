library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity full_adder_substractor_1bit is
    port ( a      : in std_logic;
           b      : in std_logic;
		   ci     : in std_logic;
		   op_sel : in std_logic;
           s      : out std_logic;
           co     : out std_logic);
end full_adder_substractor_1bit;

architecture Behavioral of full_adder_substractor_1bit is
	signal bb: std_logic;
begin
    
	bb <= b xor op_sel;
	s <= a xor bb xor ci;
	co <= (a and bb) or (a and ci) or (bb and ci);

end Behavioral;
