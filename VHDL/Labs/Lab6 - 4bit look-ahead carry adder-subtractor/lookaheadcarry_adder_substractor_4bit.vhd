library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity lookaheadcarry_adder_substractor_4bit is
    port ( A      : in std_logic_vector(3 downto 0);
           B      : in std_logic_vector(3 downto 0);
		   OP_SEL : in std_logic;
           S      : out std_logic_vector(3 downto 0);
           CO     : out std_logic);
end lookaheadcarry_adder_substractor_4bit;

architecture Behavioral of lookaheadcarry_adder_substractor_4bit is
	component lookahead_block is
		port ( a      : in std_logic;
			   b      : in std_logic;
			   ci     : in std_logic;
			   op_sel : in std_logic;
			   g      : out std_logic;
			   p      : out std_logic;
			   s      : out std_logic);
	end component;
	signal G,P: std_logic_vector(3 downto 0);
	signal C: std_logic_vector(4 downto 0);
begin
	C(0) <= OP_SEL;
	C(1) <= G(0) or (P(0) and C(0));
	C(2) <= G(1) or (P(1) and G(0)) or (P(1) and P(0) and C(0));
	C(3) <= G(2) or (P(2) and G(1)) or (P(2) and P(1) and G(0)) or (P(2) and P(1) and P(0) and C(0));
	C(4) <= G(3) or (P(3) and G(2)) or (P(3) and P(2) and G(1)) or (P(3) and P(2) and P(1) and G(0)) or (P(3) and P(2) and P(1) and P(0) and C(0));
	
	lb0: lookahead_block port map(A(0),B(0),C(0),OP_SEL,G(0),P(0),S(0));
	lb1: lookahead_block port map(A(1),B(1),C(1),OP_SEL,G(1),P(1),S(1));
	lb2: lookahead_block port map(A(2),B(2),C(2),OP_SEL,G(2),P(2),S(2));
	lb3: lookahead_block port map(A(3),B(3),C(3),OP_SEL,G(3),P(3),S(3));
	
	CO <= C(4);
end Behavioral;
