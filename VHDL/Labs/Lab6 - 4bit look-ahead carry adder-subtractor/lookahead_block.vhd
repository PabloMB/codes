library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity lookahead_block is
    port ( a      : in std_logic;
           b      : in std_logic;
		   ci     : in std_logic;
		   op_sel : in std_logic;
           g      : out std_logic;
           p      : out std_logic;
		   s      : out std_logic);
end lookahead_block;

architecture Behavioral of lookahead_block is
	signal bb: std_logic;
begin
    
	bb <= b xor op_sel;
	s <= a xor bb xor ci;
	g <= a and b;
	p <= a or b;

end Behavioral;
