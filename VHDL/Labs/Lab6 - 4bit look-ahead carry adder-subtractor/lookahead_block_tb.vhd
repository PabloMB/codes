library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity lookahead_block_tb is

end lookahead_block_tb;

architecture Behavioral of lookahead_block_tb is
	component lookahead_block is
		port ( a      : in std_logic;
			   b      : in std_logic;
			   ci     : in std_logic;
			   op_sel : in std_logic;
			   g      : out std_logic;
			   p      : out std_logic;
			   s      : out std_logic);
	end component;
	signal a,b,ci,op_sel,g,p,s: std_logic;
begin
	lb: lookahead_block port map(a,b,ci,op_sel,g,p,s);
	
	process
    begin
		a <= 'X';
        b <= 'X';
        ci <= 'X';
        op_sel <= 'X';
		wait for 1 ns;
		
		op_sel <= '0';
		
		a <= '0';
        b <= '0';
        ci <= '0';
        wait for 1 ns;
		
		a <= '0';
        b <= '1';
        ci <= '0';
        wait for 1 ns;
		
		a <= '1';
        b <= '1';
        ci <= '1';
        wait for 1 ns;
		
		a <= '1';
        b <= '0';
        ci <= '1';
        wait for 1 ns;
		
		wait for 3 ns;
		op_sel <= '1';
		
		a <= '0';
        b <= '0';
        ci <= '0';
        wait for 1 ns;
		
		a <= '0';
        b <= '1';
        ci <= '0';
        wait for 1 ns;
		
		a <= '1';
        b <= '1';
        ci <= '1';
        wait for 1 ns;
		
		a <= '1';
        b <= '0';
        ci <= '1';
        wait for 1 ns;
		
        assert false report "end of test bench";
		wait;
		
 end process;

end Behavioral;
