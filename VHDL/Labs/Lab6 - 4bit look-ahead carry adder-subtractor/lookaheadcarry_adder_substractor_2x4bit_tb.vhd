library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity lookaheadcarry_adder_substractor_2x4bit_tb is

end lookaheadcarry_adder_substractor_2x4bit_tb;

architecture Behavioral of lookaheadcarry_adder_substractor_2x4bit_tb is
	component lookaheadcarry_adder_substractor_2x4bit is
		port ( A      : in std_logic_vector(7 downto 0);
			   B      : in std_logic_vector(7 downto 0);
			   OP_SEL : in std_logic;
			   S      : out std_logic_vector(7 downto 0);
			   CO     : out std_logic);
	end component;
	signal A,B,S: std_logic_vector(7 downto 0);
	signal OP_SEL,CO: std_logic;
begin
	las2x4b: lookaheadcarry_adder_substractor_2x4bit port map(A,B,OP_SEL,S,CO);
	
	process
    begin
		A <= "XXXXXXXX";
        B <= "XXXXXXXX";
        OP_SEL <= 'X';
		wait for 1 ns;
		
		OP_SEL <= '0';
		
		A <= "00000000";
        B <= "00000000";
		wait for 1 ns;
		
		A <= "10101010";
        B <= "01010101";
		wait for 1 ns;
		
		A <= "10011001";
        B <= "01110111";
		wait for 1 ns;
		
		A <= "11111111";
        B <= "00010001";
		wait for 1 ns;
		
		wait for 3 ns;
		OP_SEL <= '1';
		
		A <= "00000000";
        B <= "00000000";
		wait for 1 ns;
		
		A <= "10101010";
        B <= "01010101";
		wait for 1 ns;
		
		A <= "10011001";
        B <= "01110111";
		wait for 1 ns;
		
		A <= "11111111";
        B <= "00010001";
		wait for 1 ns;
		
        assert false report "end of test bench";
		wait;
		
 end process;

end Behavioral;
