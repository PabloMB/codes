library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity lookaheadcarry_adder_substractor_2x4bit is
    port ( A      : in std_logic_vector(7 downto 0);
           B      : in std_logic_vector(7 downto 0);
		   OP_SEL : in std_logic;
           S      : out std_logic_vector(7 downto 0);
           CO     : out std_logic);
end lookaheadcarry_adder_substractor_2x4bit;

architecture Behavioral of lookaheadcarry_adder_substractor_2x4bit is
	component lookaheadcarry_adder_substractor_4bit is
		port ( A      : in std_logic_vector(3 downto 0);
			   B      : in std_logic_vector(3 downto 0);
			   OP_SEL : in std_logic;
			   S      : out std_logic_vector(3 downto 0);
			   CO     : out std_logic);
	end component;
	signal Cinternal: std_logic;
begin
	
	las4b_1: lookaheadcarry_adder_substractor_4bit
				port map(A(3 downto 0),B(3 downto 0),OP_SEL,
						 S(3 downto 0),Cinternal);
	las4b_2: lookaheadcarry_adder_substractor_4bit
				port map(A(7 downto 4),B(7 downto 4),Cinternal,
						 S(7 downto 4),CO);

end Behavioral;
