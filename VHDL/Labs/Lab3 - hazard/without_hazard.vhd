library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity logic_function is
    Port ( A : in std_logic;
           B : in std_logic;
           C : in std_logic;
           D : in std_logic;
           F : out std_logic);
end logic_function;

architecture Behavioral of logic_function is
signal s1, s2, s3: std_logic;
signal A_L, C_L: std_logic;
begin
    
    A_L <= not A after 5 ns;
    C_L <= not C after 5 ns;
	
	s1 <= A and C_L after 5 ns;
    s2 <= A_L and D after 5 ns;
	s3 <= C_L and D after 5 ns; --to avoid hazard
	
	F <= s1 or s2 or s3 after 5 ns;

end Behavioral;
