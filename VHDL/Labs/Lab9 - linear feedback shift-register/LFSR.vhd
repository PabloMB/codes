----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2018 13:19:08
-- Design Name: 
-- Module Name: LFSR - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LFSR is
    port (  key      : in std_logic_vector(7 downto 0);
            data_in  : in std_logic_vector(7 downto 0);
            clk      : in std_logic;
            en       : in std_logic;
            set      : in std_logic;
            data_out : out std_logic_vector(7 downto 0));
end LFSR;

architecture Behavioral of LFSR is
signal key_to_use: std_logic_vector(7 downto 0):="00000000";
signal data_to_use: std_logic_vector(7 downto 0):="00000000";
begin
    
    process(clk)
    begin
        if clk'event and clk='1' then
            if en='1' then
                data_to_use <= data_in;
                if set='1' then
                    key_to_use(7) <= key(0);
                    key_to_use(6) <= key(7);
                    key_to_use(5) <= key(6) xnor key_to_use(0);
                    key_to_use(4) <= key(5) xnor key_to_use(0);
                    key_to_use(3) <= key(4) xnor key_to_use(0);
                    key_to_use(2) <= key(3);
                    key_to_use(1) <= key(2);
                    key_to_use(0) <= key(1);
                else
                    key_to_use(7) <= key_to_use(0);
                    key_to_use(6) <= key_to_use(7);
                    key_to_use(5) <= key_to_use(6) xnor key_to_use(0);
                    key_to_use(4) <= key_to_use(5) xnor key_to_use(0);
                    key_to_use(3) <= key_to_use(4) xnor key_to_use(0);
                    key_to_use(2) <= key_to_use(3);
                    key_to_use(1) <= key_to_use(2);
                    key_to_use(0) <= key_to_use(1);
                end if;
            else
                key_to_use <= "00000000";
                data_to_use <= "00000000";
            end if;
        end if;
    end process;
    
    data_out(7) <= data_to_use(7) xor key_to_use(7);
    data_out(6) <= data_to_use(6) xor key_to_use(6);
    data_out(5) <= data_to_use(5) xor key_to_use(5);
    data_out(4) <= data_to_use(4) xor key_to_use(4);
    data_out(3) <= data_to_use(3) xor key_to_use(3);
    data_out(2) <= data_to_use(2) xor key_to_use(2);
    data_out(1) <= data_to_use(1) xor key_to_use(1);
    data_out(0) <= data_to_use(0) xor key_to_use(0);
    
end Behavioral;
