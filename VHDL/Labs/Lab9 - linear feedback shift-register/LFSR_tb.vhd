library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LFSR_tb is

end LFSR_tb;

architecture Behavioral of LFSR_tb is
    component LFSR is
       port (  key      : in std_logic_vector(7 downto 0);
               data_in  : in std_logic_vector(7 downto 0);
               clk      : in std_logic;
               en       : in std_logic;
               set      : in std_logic;
               data_out : out std_logic_vector(7 downto 0));
    end component;
signal CLK: std_logic:='0';
constant CLK_PERIOD: time := 1 ns;
signal key,data_in,data_out: std_logic_vector(7 downto 0);
signal en,set: std_logic;
begin
L1: LFSR port map(key,data_in,CLK,en,set,data_out);

    process
    begin
        CLK <= '0';
        wait for CLK_PERIOD/2;
        CLK <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    process
    begin
        
        en <= '0';
        set <= '0';
        data_in <= "01100111";
        key <= "00110100";
        wait for 5 ns;
        
        en <= '1';
        set <= '1';
        data_in <= x"45";
        key <= "00110100";
        wait for CLK_PERIOD;
        
        set <= '0';
        data_in <= x"43";
        wait for CLK_PERIOD;
        
        data_in <= x"45";
        wait for CLK_PERIOD;
        
        en <= '0';
        wait for 5 ns;
        
    end process;

end Behavioral;
