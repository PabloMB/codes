library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity simple_code_converter is
    Port ( A3 : in STD_LOGIC;
           A2 : in STD_LOGIC;
           A1 : in STD_LOGIC;
           A0 : in STD_LOGIC;
           Y3 : out STD_LOGIC;
           Y2 : out STD_LOGIC;
           Y1 : out STD_LOGIC;
           Y0 : out STD_LOGIC);
end simple_code_converter;

architecture Behavioral of simple_code_converter is

begin
    
    Y3 <= A3 or (A2 and (A0 or A1));
    Y2 <= ((not A2) and (A0 or A1)) or ((A2 and (not A1)) and (not A0));
    Y1 <= ((not(A1)) and (not(A0))) or (A1 and A0);
    Y0 <= not(A0);

end Behavioral;
