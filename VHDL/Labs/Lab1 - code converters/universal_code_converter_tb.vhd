library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity universal_code_converter_tb is
    
end universal_code_converter_tb;

architecture Behavioral of universal_code_converter_tb is
    component universal_code_converter is
        Port ( A3 : in STD_LOGIC;
			   A2 : in STD_LOGIC;
			   A1 : in STD_LOGIC;
			   A0 : in STD_LOGIC;
			   SEL : in STD_LOGIC;
			   Y3 : out STD_LOGIC;
			   Y2 : out STD_LOGIC;
			   Y1 : out STD_LOGIC;
			   Y0 : out STD_LOGIC);
    end component;
signal in3,in2,in1,in0,sel,out3,out2,out1,out0: std_logic;
signal in4b: std_logic_vector(3 downto 0):="0000";
    
begin
scc: universal_code_converter port map(in3,in2,in1,in0,sel,out3,out2,out1,out0);  
    
    process
    begin
        sel <= '0';
        
        in3 <= 'X';
        in2 <= 'X';
        in1 <= 'X';
        in0 <= 'X';
        wait for 1 ns;
    
        for I in 0 to 9 loop
            in3 <= in4b(3);
            in2 <= in4b(2);
            in1 <= in4b(1);
            in0 <= in4b(0);
            in4b <= std_logic_vector(to_unsigned(to_integer(unsigned( in4b )) + 1, 4));
            wait for 1 ns;
        end loop;
        
        sel <= '1';
        in3 <= 'X';
        in2 <= 'X';
        in1 <= 'X';
        in0 <= 'X';
        in4b <= "0011";
        wait for 1 ns;
        
        for I in 1 to 9 loop
            in3 <= in4b(3);
            in2 <= in4b(2);
            in1 <= in4b(1);
            in0 <= in4b(0);
            in4b <= std_logic_vector(to_unsigned(to_integer(unsigned( in4b )) + 1, 4));
            wait for 1 ns;
        end loop;
        
    end process;
    
end Behavioral;
