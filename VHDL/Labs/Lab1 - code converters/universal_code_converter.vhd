library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity universal_code_converter is
    Port ( A3 : in STD_LOGIC;
           A2 : in STD_LOGIC;
           A1 : in STD_LOGIC;
           A0 : in STD_LOGIC;
		   SEL : in STD_LOGIC;
           Y3 : out STD_LOGIC;
           Y2 : out STD_LOGIC;
           Y1 : out STD_LOGIC;
           Y0 : out STD_LOGIC);
end universal_code_converter;

architecture Behavioral of universal_code_converter is
begin

	process(A3,A2,A1,A0,SEL)
	begin
		if SEL='0' then
			Y3 <= A3 or (A2 and (A0 or A1));
            Y2 <= ((not A2) and (A0 or A1)) or ((A2 and (not A1)) and (not A0));
            Y1 <= ((not(A1)) and (not(A0))) or (A1 and A0);
            Y0 <= not(A0);
		else
			Y3 <= (A3 and A2) or (A3 and A1 and A0);
			Y2 <= (not(A2) and not(A1)) or (A2 and A1 and A0) or (not(A2) and not(A0));
			Y1 <= ((not(A1)) and A0) or (A1 and (not(A0)));
			Y0 <= not(A0);
		end if;
	end process;

end Behavioral;
