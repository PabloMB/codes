library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity simple_code_converter_tb is

end simple_code_converter_tb;

architecture Behavioral of simple_code_converter_tb is
    component simple_code_converter is
        port ( A3 : in std_logic;
			   A2 : in std_logic;
			   A1 : in std_logic;
			   A0 : in std_logic;
			   Y3 : out std_logic;
			   Y2 : out std_logic;
			   Y1 : out std_logic;
			   Y0 : out std_logic);
    end component;
    
signal in3,in2,in1,in0,out3,out2,out1,out0: std_logic;
signal in4b: std_logic_vector(3 downto 0):="0000";

begin
scc: simple_code_converter port map(in3,in2,in1,in0,out3,out2,out1,out0);     

    process
    begin
    in3 <= 'X';
    in2 <= 'X';
    in1 <= 'X';
    in0 <= 'X';
    wait for 1 ns;
    
        for I in 0 to 9 loop
            in3 <= in4b(3);
            in2 <= in4b(2);
            in1 <= in4b(1);
            in0 <= in4b(0);
            in4b <= std_logic_vector(to_unsigned(to_integer(unsigned( in4b )) + 1, 4));
            wait for 1 ns;
        end loop;
        
    end process;

end Behavioral;
