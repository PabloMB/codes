library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity flipflop_d_tb is

end flipflop_d_tb;

architecture Behavioral of flipflop_d_tb is
    component flipflop_d is
        port ( D   : in std_logic;
               CLK : in std_logic;
               Q   : out std_logic;
               Q_L : out std_logic);
    end component;
signal CLK: std_logic:='0';
constant CLK_PERIOD: time := 2 ns;
signal D,Q,Q_L: std_logic;
begin
ffd: flipflop_d port map(D,CLK,Q,Q_L);
    
    process
    begin
        CLK <= '0';
        wait for CLK_PERIOD/2;
        CLK <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    process
    begin
        
        wait for 0.5 ns;
        
        D <= '1';
        wait for 2 ns;
        
        D <= '0';
        wait for 2 ns;
        
        D <= '1';
        wait for 2 ns;
        
        D <= '0';
        wait for 2 ns;
        
        wait for 0.5 ns;
        
    end process;

end Behavioral;
