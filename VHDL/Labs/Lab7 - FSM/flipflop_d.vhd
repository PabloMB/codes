library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity flipflop_d is
    port ( D   : in std_logic;
           CLK : in std_logic;
           Q   : out std_logic;
           Q_L : out std_logic);
end flipflop_d;

architecture Behavioral of flipflop_d is

begin

    process(D,CLK)
    begin
        if clk'event and clk='1' then
            Q <= D;
            Q_L <= not(D);
        end if;
    end process;
    
end Behavioral;
