library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity turn_signal_FSM is
    port ( L : in std_logic;
           R : in std_logic;
           CLK : in std_logic;
           LL : out std_logic;
           HL : out std_logic;
           RL : out std_logic);
end turn_signal_FSM;

architecture Behavioral of turn_signal_FSM is
    component flipflop_d is
        port ( D   : in std_logic;
               CLK : in std_logic;
               Q   : out std_logic;
               Q_L : out std_logic);
    end component;
    signal Q2,Q1,Q0: std_logic;
    signal Q2_L,Q1_L,Q0_L: std_logic;
    signal D2,D1,D0: std_logic;
begin
ffd2: flipflop_d port map(D2,CLK,Q2,Q2_L);
ffd1: flipflop_d port map(D1,CLK,Q1,Q1_L);
ffd0: flipflop_d port map(D0,CLK,Q0,Q0_L);

    D2 <= Q2_L and Q1_L and Q0_L and L;
    D1 <= Q2 and Q0;
    D0 <= Q2_L and Q1_L and Q0_L and R;
    
    LL <= Q2;
    HL <= Q1;
    RL <= Q0;

end Behavioral;
