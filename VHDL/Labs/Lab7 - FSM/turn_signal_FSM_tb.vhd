library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity turn_signal_FSM_tb is

end turn_signal_FSM_tb;

architecture Behavioral of turn_signal_FSM_tb is
    component turn_signal_FSM is
        port ( L : in std_logic;
               R : in std_logic;
               CLK : in std_logic;
               LL : out std_logic;
               HL : out std_logic;
               RL : out std_logic);
    end component;
signal CLK: std_logic:='0';
constant CLK_PERIOD: time := 1 ns;
signal L,R,LL,HL,RL: std_logic;
--type state_type is (IDLE,LSIG,RSIG,H1,H2,ERROR);
--signal state : state_Type;
--signal Q: std_logic_vector(2 downto 0);
begin
tsFSM: turn_signal_FSM port map(L,R,CLK,LL,HL,RL);

    process
    begin
        CLK <= '0';
        wait for CLK_PERIOD/2;
        CLK <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
    process
    begin
        
        wait for 0.25 ns;
        
        L <= '0';
        R <= '0';
        wait for 4 ns;
        
        L <= '1';
        R <= '0';
        wait for 8 ns;
        
        L <= '0';
        R <= '1';
        wait for 8 ns;
        
        L <= '1';
        R <= '1';
        wait for 8 ns;
        
        L <= '0';
        R <= '0';
        wait for 4 ns;
        
        wait for 0.75 ns;
        
    end process;
    
--    process
    
--    begin
--        Q(2) <= Q2;
--        Q(1) <= Q1;
--        Q(0) <= Q0;
--        if Q="000" then state <= IDLE;
--        elsif Q="100" then state <= LSIG;
--        elsif Q="001" then state <= RSIG;
--        elsif Q="101" then state <= H1;
--        elsif Q="010" then state <= H2;
--        else state <= ERROR;
--        end if;
--    end process;


end Behavioral;
