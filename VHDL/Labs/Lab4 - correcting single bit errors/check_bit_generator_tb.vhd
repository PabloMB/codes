library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity check_bit_generator_tb is

end check_bit_generator_tb;

architecture Behavioral of check_bit_generator_tb is
	component check_bit_generator is
		port ( D1 : in std_logic;
               D2 : in std_logic;
               D3 : in std_logic;
               D4 : in std_logic;
               P1 : in std_logic;
               P2 : in std_logic;
               P4 : in std_logic;
               C1 : out std_logic;
               C2 : out std_logic;
               C4 : out std_logic;
               DD1 : out std_logic;
               DD2 : out std_logic;
               DD3 : out std_logic;
               DD4 : out std_logic);
	end component;
signal D1,D2,D3,D4,P1,P2,P4,C1,C2,C4,DD1,DD2,DD3,DD4: std_logic;
begin
cbg: check_bit_generator port map(D1,D2,D3,D4,P1,P2,P4,C1,C2,C4,DD1,DD2,DD3,DD4);
	
	process
    begin
		D1 <= 'X';
        D2 <= 'X';
        D3 <= 'X';
        D4 <= 'X';
		P1 <= 'X';
        P2 <= 'X';
        P4 <= 'X';
		wait for 1 ns;
		
        P1 <= '0';
		P2 <= '0';
		D1 <= '0';
        P4 <= '0';
		D2 <= '0';
        D3 <= '0';
        D4 <= '0';
		wait for 1 ns;
		
        P1 <= '1';
		P2 <= '1';
		D1 <= '1';
        P4 <= '0';
		D2 <= '0';
        D3 <= '0';
        D4 <= '0';
        wait for 1 ns;
		
        P1 <= '1';
		P2 <= '1';
		D1 <= '0';
        P4 <= '0';
		D2 <= '1';
        D3 <= '1';
        D4 <= '0';
        wait for 1 ns;
		
        P1 <= '0';
		P2 <= '0';
		D1 <= '0';
        P4 <= '1';
		D2 <= '1';
        D3 <= '1';
        D4 <= '1';
        wait for 1 ns;
		
		P1 <= '0';
		P2 <= '0';
		D1 <= '0';
        P4 <= '1';
		D2 <= '1';
        D3 <= '1';
        D4 <= '0';
        wait for 1 ns;
		
 end process;

end Behavioral;
