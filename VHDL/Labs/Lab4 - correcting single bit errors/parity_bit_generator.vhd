library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity parity_bit_generator is
    port ( D1 : in std_logic;
           D2 : in std_logic;
           D3 : in std_logic;
           D4 : in std_logic;
           DD1 : out std_logic;
           DD2 : out std_logic;
           DD3 : out std_logic;
           DD4 : out std_logic;
           P1 : out std_logic;
           P2 : out std_logic;
           P4 : out std_logic);
end parity_bit_generator;

architecture Behavioral of parity_bit_generator is
signal B1,B2,B3,B4,B5,B6,B7: std_logic;
begin

	B3 <= D1;
	B5 <= D2;
	B6 <= D3;
	B7 <= D4;
	
	DD1 <= D1;
    DD2 <= D2;
    DD3 <= D3;
    DD4 <= D4;
    
	P1 <= B3 xor B5 xor B7;
	P2 <= B3 xor B6 xor B7;
	P4 <= B5 xor B6 xor B7;

end Behavioral;