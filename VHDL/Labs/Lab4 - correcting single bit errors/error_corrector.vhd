library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity error_corrector is
    port ( D1 : in std_logic;
           D2 : in std_logic;
           D3 : in std_logic;
           D4 : in std_logic;
           C1 : in std_logic;
           C2 : in std_logic;
           C4 : in std_logic;
		   DC1 : out std_logic;
           DC2 : out std_logic;
		   DC3 : out std_logic;
           DC4 : out std_logic);
end error_corrector;

architecture Behavioral of error_corrector is
signal B1,B2,B3,B4,B5,B6,B7: std_logic;
signal C: std_logic_vector(2 downto 0);
signal R1,R2,R3,R4,R5,R6,R7: std_logic;
begin
	B1 <= C1;
	B2 <= C2;
	B3 <= D1;
	B4 <= C4;
	B5 <= D2;
	B6 <= D3;
	B7 <= D4;
	
	C(0) <= C1;
	C(1) <= C2;
	C(2) <= C4;
	
	process(C)
	begin
		if(C="000") then
			R1<=B1;
			R2<=B2;
			R3<=B3;
			R4<=B4;
			R5<=B5;
			R6<=B6;
			R7<=B7;
		elsif(C="001") then
			R1<=not(B1);
			R2<=B2;
			R3<=B3;
			R4<=B4;
			R5<=B5;
			R6<=B6;
			R7<=B7;
		elsif(C="010") then
			R1<=B1;
			R2<=not(B2);
			R3<=B3;
			R4<=B4;
			R5<=B5;
			R6<=B6;
			R7<=B7;
		elsif(C="011") then
			R1<=B1;
			R2<=B2;
			R3<=not(B3);
			R4<=B4;
			R5<=B5;
			R6<=B6;
			R7<=B7;
		elsif(C="100") then
			R1<=B1;
			R2<=B2;
			R3<=B3;
			R4<=not(B4);
			R5<=B5;
			R6<=B6;
			R7<=B7;
		elsif(C="101") then
			R1<=B1;
			R2<=B2;
			R3<=B3;
			R4<=B4;
			R5<=not(B5);
			R6<=B6;
			R7<=B7;
		elsif(C="110") then
			R1<=B1;
			R2<=B2;
			R3<=B3;
			R4<=B4;
			R5<=B5;
			R6<=not(B6);
			R7<=B7;
		elsif(C="111") then
			R1<=B1;
			R2<=B2;
			R3<=B3;
			R4<=B4;
			R5<=B5;
			R6<=B6;
			R7<=not(B7);
		end if;
	end process;
	
	DC1 <= R3;
	DC2 <= R5;
	DC3 <= R6;
	DC4 <= R7;
end Behavioral;
