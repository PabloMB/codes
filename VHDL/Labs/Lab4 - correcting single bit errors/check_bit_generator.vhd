library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity check_bit_generator is
    port ( D1 : in std_logic;
           D2 : in std_logic;
           D3 : in std_logic;
           D4 : in std_logic;
           P1 : in std_logic;
           P2 : in std_logic;
           P4 : in std_logic;
		   C1 : out std_logic;
           C2 : out std_logic;
           C4 : out std_logic;
           DD1 : out std_logic;
           DD2 : out std_logic;
           DD3 : out std_logic;
           DD4 : out std_logic);
end check_bit_generator;

architecture Behavioral of check_bit_generator is
signal B1,B2,B3,B4,B5,B6,B7: std_logic;
begin
	B1 <= P1;
	B2 <= P2;
	B3 <= D1;
	B4 <= P4;
	B5 <= D2;
	B6 <= D3;
	B7 <= D4;
	
	C1 <= B1 xor B3 xor B5 xor B7;
	C2 <= B2 xor B3 xor B6 xor B7;
	C4 <= B4 xor B5 xor B6 xor B7;
	
	DD1 <= D1;
    DD2 <= D2;
    DD3 <= D3;
    DD4 <= D4;

end Behavioral;

