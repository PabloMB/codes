library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity error_corrector_tb is

end error_corrector_tb;

architecture Behavioral of error_corrector_tb is
	component error_corrector is
		port ( D1 : in std_logic;
			   D2 : in std_logic;
			   D3 : in std_logic;
			   D4 : in std_logic;
			   C1 : in std_logic;
			   C2 : in std_logic;
			   C4 : in std_logic;
			   DC1 : out std_logic;
			   DC2 : out std_logic;
			   DC3 : out std_logic;
			   DC4 : out std_logic);
	end component;
signal D1,D2,D3,D4,C1,C2,C4,DC1,DC2,DC3,DC4: std_logic;
begin
ec: error_corrector port map(D1,D2,D3,D4,C1,C2,C4,DC1,DC2,DC3,DC4);
	
	process
    begin
		D1 <= 'X';
        D2 <= 'X';
        D3 <= 'X';
        D4 <= 'X';
		C1 <= 'X';
        C2 <= 'X';
        C4 <= 'X';
		wait for 1 ns;
		
        C1 <= '0';
		C2 <= '0';
		D1 <= '0';
        C4 <= '1';
		D2 <= '0';
        D3 <= '0';
        D4 <= '0';
		wait for 1 ns;
		
        C1 <= '0';
		C2 <= '1';
		D1 <= '0';
        C4 <= '1';
		D2 <= '0';
        D3 <= '1';
        D4 <= '1';
        wait for 1 ns;
		
        C1 <= '1';
		C2 <= '1';
		D1 <= '0';
        C4 <= '1';
		D2 <= '1';
        D3 <= '0';
        D4 <= '1';
        wait for 1 ns;
		
        C1 <= '1';
		C2 <= '0';
		D1 <= '1';
        C4 <= '0';
		D2 <= '1';
        D3 <= '1';
        D4 <= '1';
        wait for 1 ns;
		
		C1 <= '1';
		C2 <= '0';
		D1 <= '0';
        C4 <= '0';
		D2 <= '0';
        D3 <= '0';
        D4 <= '0';
        wait for 1 ns;
        
    end process;

end Behavioral;
