library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity parity_bit_generator_tb is

end parity_bit_generator_tb;

architecture Behavioral of parity_bit_generator_tb is
	component parity_bit_generator is
		port ( D1 : in std_logic;
               D2 : in std_logic;
               D3 : in std_logic;
               D4 : in std_logic;
               DD1 : out std_logic;
               DD2 : out std_logic;
               DD3 : out std_logic;
               DD4 : out std_logic;
               P1 : out std_logic;
               P2 : out std_logic;
               P4 : out std_logic);
	end component;
signal D1,D2,D3,D4,DD1,DD2,DD3,DD4,P1,P2,P4: std_logic;
begin
pbg: parity_bit_generator port map(D1,D2,D3,D4,DD1,DD2,DD3,DD4,P1,P2,P4);
	
	process
    begin
		D1 <= 'X';
        D2 <= 'X';
        D3 <= 'X';
        D4 <= 'X';
		wait for 1 ns;
		
        D1 <= '0';
        D2 <= '0';
        D3 <= '0';
        D4 <= '0';
        wait for 1 ns;
		
        D1 <= '0';
        D2 <= '0';
        D3 <= '1';
        D4 <= '0';
        wait for 1 ns;
		
        D1 <= '1';
        D2 <= '0';
        D3 <= '1';
        D4 <= '0';
        wait for 1 ns;
		
        D1 <= '1';
        D2 <= '1';
        D3 <= '1';
        D4 <= '1';
        wait for 1 ns;
		
	end process;

end Behavioral;
