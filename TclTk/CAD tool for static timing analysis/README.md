# CAD tool for static timing analysis (STA)

Project developed for the course ECE588 CAD Techniques for VLSI Design at IIT
  
##Explanation

Static timing analysis (STA) is a method for calculating the timing of a digital circuit without considering its functioning. It is a faster method than the simulation of the full circuit.  

Using the time needed for each node to complete (which will be called delay time), it checks every possible path to calculate the earliest finalization time (arrival time), the latest finalization time (required time) and the difference between these two (slack time).  

The calculation is done in three steps:
1)	all paths are traversed from start to end to determine the arrival times of each node,  
2)	the same is done from end to start to determine the required times,  
3)	both values of each node are subtracted to determine the slack time.  

In some way, this slack time points out the freedom that a node (which represents a process in a real circuit or an activity in a real project) has to start and end earlier or later (i.e. a slack equal to zero indicates that the activity cannot be delayed).  
  
##Instructions

- Having Tcl installed on the computer, double click on the file "STA tool.tcl".

- A window will open with only two unconnected nodes: the first node of the graph (PI) and the last one (PO).

- Clicking on PI (or on other nodes later), a second window will open where you can:  
-- add a child for the clicked node,  
-- join the clicked node to an existing node,  
-- delete the connection(s) that start at that node,  
-- delete the clicked node (so that no nodes are left alone and all nodes are connected to PI, except PO, all the paths that start at that node are erased, so this must be done carefully),  
-- or enter the delay of the clicked node.  
![Scheme](images/sequence_menu.png)  

- Once you finish creating the graph and inserting the delay of every node (except PI and PI which can only be 0), you can click on the button "Calculate slacks".

- A file called "inputs_dfs.txt" will be created to save the information of the graph and the file "dfs_getresults.exe" will be called to obtain the resulting times, which will be stored in "results.txt".

- The results will be shown on screen and the nodes on the critical path wil colored in red.

Note 1: the button "Reset graph" can be clicked at any time to go to the starting position.  

Note 2: the button "EXIT" can be clicked to close this STA tool.  
  
  
  
**Example sequence**  
  
![Scheme](images/sequence01.png)  
![Scheme](images/sequence02.png)  
![Scheme](images/sequence03.png)  
![Scheme](images/sequence04.png)  
![Scheme](images/sequence05.png)  
![Scheme](images/sequence06.png)  
![Scheme](images/sequence07.png)  
![Scheme](images/sequence08.png)  
![Scheme](images/sequence09.png)  
![Scheme](images/sequence10.png)  
![Scheme](images/sequence11.png)  
![Scheme](images/sequence12.png)  
  
The button “Reset graph” can be clicked at any time to go to the starting position:  
  
![Scheme](images/sequence_restart.png)  
  
The button “EXIT” can also be clicked to close this CAD tool.  
  
The final window with the end of the previous sequence is shown in the next image:  
  
![Scheme](images/sequence_final.png)
  
As we can see, with the delays inserted, the total time needed is 6 and the critical path (formed by those nodes with slack equal to 0) is: PI, C, D, PO  
  
  
  
  
**The following improvements are left for future work:**  
- save the graph created to a file, so that progress can be saved  
- in combination with the previous point, load a graph stored in a file (with the structure commented in Part 1)  
- reduce the size of this file (pairs of names could be used to indicate the directed connection between pairs of nodes)  
- handle errors (e.g. if the user connects one node with a node that does not exist)  
  
  
  
  
If you find any problem, please write an email to pabmen3c08@gmail.com.  

Thank you!
