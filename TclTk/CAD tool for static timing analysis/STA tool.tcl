#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

package require Tk

set canvasWidth 1200
set canvasHeight 800
set radio 18

set basic_dist_x [expr $canvasWidth*0.1]
set basic_dist_y [expr $canvasHeight*0.1]
set orig_x [expr $canvasWidth*0.1]
set orig_y [expr $canvasHeight*0.5]
set dist_x $basic_dist_x
set dist_y $basic_dist_y

set notify_text ""

set source PI
set last PO
set slackParams ""
set slacks() ""

array set arrival {}
array set required {}
array set slack {}

set instrSlackText {Press "Calculate slacks" to obtain arrival, required and slack times}
append instrSlackText "\n(the delay of each node is shown underneath)"
append instrSlackText "\n(times are shown over nodes: arrival/required=>slack)"

foreach {key value} [array get slacks] {
	if {$value==""} {
		unset slacks($key)
	}
}

proc reset_times {} {
	global arrival
	global required
	global slack
	global xpos
	
	# global instrSlackText
	# set instrSlackText {Press "Calculate slacks" to obtain arrival, required and slack times}
	
	foreach name [array names xpos] {
		if {[info exists arrival($name)]} { unset arrival($name) }
		if {[info exists required($name)]} { unset required($name) }
		if {[info exists slack($name)]} { unset slack($name) }
	}
	update_notification ""
}

 # drawgraph.tcl --
 #    Script to draw graphs (represented as edgelist) in a canvas
 #

 # DrawGraph --
 #    Namespace for the commands
 #
 namespace eval ::DrawGraph:: {
    variable draw_vertex  "DrawVertex"
    variable draw_edge    "DrawEdge"
    variable curved       0
    variable directed     0
 }

 # DrawVertex --
 #    Default vertex drawing routine
 # Arguments:
 #    canvas    Canvas to draw on
 #    xv        X coordinate
 #    yv        Y coordinate
 #    name      Name of the vertex
 
 # Output:
 #    Filled circle drawn at vertex
 #
 proc ::DrawGraph::DrawVertex { canvas xv yv name } {
	global radio
    $canvas create oval [expr {$xv-$radio}] [expr {$yv-$radio}] \
                        [expr {$xv+$radio}] [expr {$yv+$radio}] \
						-fill white -tag "oval$name"
	
	global slack
	if {[info exists slack($name)]} {
		if {$slack($name)==0} {
			.c1 itemconfigure "oval$name" -fill {red}
		} else {
			.c1 itemconfigure "oval$name" -fill {light blue}
		}
	}
		
  $canvas create text $xv $yv -text $name -tag $name
  $canvas bind $name <1> "enterSlack $name"
 }
 
 proc draw_vertex { canvas x y name } {
	::DrawGraph::DrawVertex $canvas $x $y $name
 }

# DrawEdge --
 #    Default edge drawing routine
 # Arguments:
 #    canvas    Canvas to draw on
 #    xb        X coordinate begin
 #    yb        Y coordinate begin
 #    xe        X coordinate end
 #    ye        Y coordinate end
 #    curved    Draw a curved edge or not
 #    directed  Draw an arrow head or not
 #    attrib    Attribute of the vertex

 # Output:
 #    Line from the beginning to the end
 #
 
 proc ::DrawGraph::DrawEdge { canvas xb yb xe ye curved directed attrib } {
    if { $directed } {
       set arrowtype last
    } else {
       set arrowtype none
    }
	
	global radio
	set xb [expr $xb+$radio]
	set xe [expr $xe-$radio]
	
    set dx [expr {$xe-$xb}]
    set dy [expr {$ye-$yb}]
    if { ! $curved } {
       set xc [expr {$xb+0.5*$dx}]
       set yc [expr {$yb+0.5*$dy}]
    } else {
       set xc [expr {$xb+0.5*$dx-0.1*$dy}]
       set yc [expr {$yb+0.5*$dy+0.1*$dx}]
    }
    $canvas create line $xb $yb $xc $yc $xe $ye -fill black \
       -arrow $arrowtype -smooth $curved
 }
 
 proc draw_edge { canvas x1 y1 x2 y2} {
	::DrawGraph::DrawEdge .c1 $x1 $y1 $x2 $y2 0 1 0
 }

   
   proc draw_graph {canvas} {
		.c1 delete all
		global canvasWidth
        global canvasHeight
		global xpos
		global ypos
		
		global orig_x
		global orig_y
		global dist_x
		global dist_y
		global radio
		global slacks
		global arrival
	    global required
	    global slack
		
		#draw vertex and times
		foreach name [array names xpos] {
			#draw vertex
			set vx [expr $xpos($name)*$dist_x+$orig_x]
			set vy_base [expr $ypos($name)*$dist_y+$orig_y]
			set vy $vy_base
			draw_vertex $canvas $vx $vy $name
			
			#place time of node (under the node)
			if {[info exists slacks($name)]} {
				set vy [expr $vy_base+1.5*$radio]
				.c1 create text $vx $vy -text $slacks($name) -tag "time$name"
			} else {
				set vy [expr $vy_base+1.5*$radio]
				.c1 create text $vx $vy -text "-" -tag "time$name"
			}
			
			#place arrival time / required time -> slack time (over the node)
			set vy [expr $vy_base-1.5*$radio]
			set s ""
			if {[info exists arrival($name)]} { append s $arrival($name) "/"
			} else { append s "-" "/" }
			if {[info exists required($name)]} { append s $required($name) "->"
			} else { append s "-" "=>" }
			if {[info exists slack($name)]} { append s $slack($name)
			} else { append s "-" }
			.c1 create text $vx $vy -text $s -tag "times$name"
		}
		
		#draw edge
		global connection
		foreach name1 [array names xpos] {
			foreach name2 [array names xpos] {
				if {[info exists connection($name1,$name2)]} {
					if {$connection($name1,$name2)==1} {
						set x1 [expr $xpos($name1)*$dist_x+$orig_x]
						set y1 [expr $ypos($name1)*$dist_y+$orig_y]
						set x2 [expr $xpos($name2)*$dist_x+$orig_x]
						set y2 [expr $ypos($name2)*$dist_y+$orig_y]
						draw_edge canvas $x1 $y1 $x2 $y2
					}
				}
			}
		}
		
   }
   
   proc update_notification {text} {
		global notify_text
		set notify_text $text
		.c1 itemconfigure notification -text $notify_text
   }
	
   proc update_canvas {} {
		global xpos
		global ypos
		
		draw_graph .c1
		
		# set test "Calculate slacks"
		# set name "FindSlacks"
        # .c1 create text 75 20 -text $test -fill red -tag $name
        # .c1 bind $name <1> "findSlack"
		
		global canvasHeight
        global canvasWidth
		set x [expr $canvasWidth*0.2]
		set y [expr $canvasHeight*0.05]
		global instrSlackText
		.c1 create text $x $y -text $instrSlackText 
		
		set x [expr $canvasWidth*0.5]
		set y [expr $canvasHeight*0.95]
		.c1 create text $x $y -text "note" -tag notification
		.c1 itemconfigure notification
		update_notification ""
		
		.c1 create text 1000 550 -text "" -tag resultstag
   }
   
   proc reset_graph {} {
		
		reset_times
		
		global next_child
		global next_child_c
		global xpos
		global ypos
		global source
		global last
		
		set next_child 1
		set next_child_c A
		
		foreach name [array names xpos] {
			unset xpos($name)
			unset ypos($name)
		}
		
		set xpos($source) 0
		set xpos($last) 1
		set ypos($source) 0
		set ypos($last) 0
		
		update_canvas
   }
   
   proc main {} {
  
        global canvasHeight
        global canvasWidth
        
        set xaxis 100
        set yaxis [expr $canvasHeight/2]
        
		frame .f1
		frame .f2
		pack  .f1 .f2 -side bottom

        canvas .c1 -background {light gray} -relief sunken -width $canvasWidth -height $canvasHeight
		pack .c1 -in .f1
        
		button .b1 -text "Calculate slacks" -command {findSlack}
		button .b2 -text "Reset graph" -command {reset_graph}
		button .b3 -text EXIT -command {exit}
		pack .b1 .b2 .b3 -in .f2 -side left -padx 5
        
		.c1 create text 100 100 -text "hey" -tag resultstag
		
		
		reset_graph
		
		update_canvas
		
        
		
  }
  
  
 # This function will accept the input when each node is clicked
 proc dfs_reorganize {name} {
	global xpos
	global ypos
	global connection
	global last
	global min_pos_last
	foreach node [array names xpos] {
		if {[info exists connection($name,$node)]} {
			if {$connection($name,$node)==1} {
				set x [expr $xpos($name)+1]
				if {$x>$xpos($node)} {
					set ypos($node) 0
					
					foreach other [array names xpos] {
						if {$xpos($other)==$x && $ypos($other)>=$ypos($node) && $other!=$node} {
								set ypos($node) [expr $ypos($other)+1]
						}
					}
					
					set xpos($node) $x
					
					if {$x>=$min_pos_last} {
						set min_pos_last [expr $x+1]
					}
					
					dfs_reorganize $node
				}
			}
		}
	}
 }
 proc reorganize_nodes {} {
	global xpos
	global ypos
	global last
	global min_pos_last
	set min_pos_last 0
	foreach name [array names xpos] {
		set xpos($name) 0
		set ypos($name) 0
	}
	#reorganize from source node
	global source
	dfs_reorganize $source
	if {$xpos($last)==0} {
		set xpos($last) $min_pos_last
	}
	
	#place nodes symmetrically with the horizontal
	set max_y 0
	for {set i 0} {$i < $xpos($last)} {incr i} {
		set num_in_x 0
		foreach name [array names xpos] {
			if {$xpos($name)==$i} {incr num_in_x}
		}
		if {$num_in_x>$max_y} {set max_y $num_in_x}
		set modify_y [expr ($num_in_x-1)/2.0]
		foreach name [array names xpos] {
			if {$xpos($name)==$i} {set ypos($name) [expr $ypos($name)-$modify_y]}
		}
	}
	
	#avoid nodes out of the window
	global basic_dist_x
	global basic_dist_y
	global dist_x
	global dist_y
	global orig_x
	global canvasWidth
	global canvasHeight
	if {($dist_y*$max_y)>(0.9*$canvasHeight)} {
		set dist_y [expr ($canvasHeight*0.9)/$max_y]
	}
	if {($basic_dist_y*$max_y)<=(0.9*$canvasHeight)} {
		set dist_y $basic_dist_y
	}
	set max_x $xpos($last)
	if {($dist_x*$max_x+$orig_x)>(0.95*$canvasWidth)} {
		set dist_x [expr ($canvasWidth*0.95-$orig_x)/$max_x]
	}
	if {($basic_dist_x*$max_x+$orig_x)<=(0.95*$canvasWidth)} {
		set dist_x $basic_dist_x
	}
	
 }
 
 
 proc add_child {parent} {
	global next_child_c
	global xpos
	global ypos
	global last
	global connection
	
	if {$parent!=$last} {
		set connection($parent,$next_child_c) 1
		puts "connection($parent,$next_child_c) made"
		set xpos($next_child_c) 0
		set ypos($next_child_c) 0
		
		set next_child_c [format %c [expr {[scan $next_child_c %c]+1}]]
		
		reorganize_nodes
		update_canvas
	} else {
		update_notification "cannot add a child to the last node PO"
	}
 }
 proc delete_node {name} {
	global source
	global last
	if {$name!=$source && $name!=$last} {
		global xpos
		global ypos
		unset xpos($name)
		unset ypos($name)
		reorganize_nodes
		update_canvas
	}
 }
 
 proc delete_posterior_connection {name} {
	global xpos
	global ypos
	global connection
	global last
	foreach other [array names xpos] {
		if {[info exists connection($name,$other)]} {
			if {$connection($name,$other)==1} {
				unset connection($name,$other)
				if {$other!=$last} {
					delete_posterior_connection $other
					unset xpos($other)
					unset ypos($other)
				}
			}
		}
	}
	reorganize_nodes
	update_canvas
 }
 
 proc join_nodes {name1 name2} {
	global connection
	set connection($name1,$name2) 1
	puts "connection($name1,$name2) made"
	reorganize_nodes
	update_canvas
 }
 
 proc enterSlack { param } {
	
	puts "enterSlack()"
	
	  global canvasWidth
	  global canvasHeight
      set c1Width [expr $canvasWidth*0.3]
      set c1Height [expr $canvasHeight*0.3]
      global slackParams
      set slackParams $param
      
		
      toplevel .t3 -bg {light gray}
        
      frame .t3.c1 -bg {light gray} -width $c1Width -height $c1Height
	  frame .t3.c2 -bg {light gray} -width $c1Width -height $c1Height
	  frame .t3.c3 -bg {light gray} -width $c1Width -height $c1Height
	  frame .t3.c4 -bg {light gray} -width $c1Width -height $c1Height
	  pack  .t3.c1 .t3.c2 .t3.c3 .t3.c4 -in .t3 -side top
	  
	  button .t3.c1.b1 -text "Add child for $param?" -command {reset_times; add_child $slackParams; puts "new child for $slackParams"; destroy .t3}
	  pack .t3.c1.b1 -in .t3.c1 -side top
	  
	  label .t3.c2.l4 -text "Join $param to:"
      entry .t3.c2.e4 -width 20 -relief sunken -textvariable nameJoin
      button .t3.c2.b4 -text Ok -command {reset_times; join_nodes $slackParams $nameJoin; set nameJoin ""; destroy .t3}
      pack .t3.c2.l4 .t3.c2.e4 .t3.c2.b4 -in .t3.c2 -side left
	  
	  button .t3.c3.b3 -text "Delete posterior connection(s) of $param?" -command {reset_times; delete_posterior_connection $slackParams; puts "delete posterior connection(s) of $slackParams"; destroy .t3}
	  pack .t3.c3.b3 -in .t3.c3 -side top
	  
	  button .t3.c3.b2 -text "Delete node $param?" -command {reset_times; delete_node $slackParams; puts "delete node $slackParams"; destroy .t3}
	  pack .t3.c3.b2 -in .t3.c3 -side top
	  
	  label .t3.c4.l5 -text "Enter time of $param:"
      entry .t3.c4.e5 -width 20 -relief sunken -textvariable name
      button .t3.c4.b5 -text Ok -command {reset_times; set_node_time $name; set name ""; destroy .t3}
	  #bind .t3.c1.b <Key-Enter> {set_node_time $name; set name ""}
      pack .t3.c4.l5 .t3.c4.e5 .t3.c4.b5 -in .t3.c4 -side left
	  
	  .c1 itemconfigure resultstag -text ""
   }
 
   # This function will store the inputs received from enterSlack function into an array called "slacks"
   proc set_node_time { name } {
   
	  puts "set_node_time()"
		
      global slackParams
      global slacks
	  global source
	  global last
      if {$slackParams!=$source && $slackParams!=$last} {
		  set slacks($slackParams) $name
		  puts "value inserted for $slackParams: $slacks($slackParams)"
		  update_canvas
	  } else {
		  update_notification "cannot change time of $source and $last"
	  }
	  
   }  
      

#=================== To be filled by Students ==================


   proc findSlack { } {
      
	  puts "findSlack()"
	  
       global slackParams
       global slacks
	   global source
	   global last
	   global connection
		
       set canvasHeight 800
       set canvasWidth 1200
       
       # Note: This function works only if you have read all the inputs by clicking each node in the graph
       
	   #check that all nodes !=last have children
	    global xpos
	    foreach name1 [array names xpos] {
			if {$name1!=$last} {
				set has_connection 0
				foreach name2 [array names xpos] {
					if {[info exists connection($name1,$name2)]} {
						if {$connection($name1,$name2)==1} {
							set has_connection 1
						}
					}
				}
				if {$has_connection==0} {
					puts "$name1 has no connection"
					update_notification "$name1 has no connection"
					return
				}
			}
		}
	   
	   #check that all nodes have a time set
	   global next_child_c
	   set num_nodes [expr {[scan $next_child_c %c]-[scan "A" %c]}]
	   set total_num_nodes [expr $num_nodes+2]
	   puts "There are $num_nodes nodes ($total_num_nodes nodes in total)"
	   
	   set count 0
       foreach {key value} [array get slacks] {
			if { $key==$source || $key==$last} {
				unset slacks($key)
			} else {
				puts "$key,$value"
				incr count
			}
	   }
	   if {$count<$num_nodes} {
			puts "$count values set, [expr ($num_nodes-$count)] values missing"
			update_notification "$count values set, [expr ($num_nodes-$count)] values missing"
			return
		}

       
       # Step 1: Add TCL code here so that the inputs go into the file "inputs_dfs.txt" in the form of a matrix
	         # Read the inputs from the "slacks" array that was recorded by the set_node_time function
       	    	 # and write these inputs to the file "inputs_dfs.txt" (which is given below) in the form of a
	         # matrix that the C program can read.
	         # For example slacks("G") would give the delay for node "G"
	   
	   set slackfile [open "inputs_dfs.txt" w]
	   
	   puts $slackfile $total_num_nodes
	   
	   #save adjacency matrix (connections) inside the file
	   for {set i 0} {$i < $total_num_nodes} {incr i} {
			set s ""
			for {set j 0} {$j < $total_num_nodes} {incr j} {
				if {$i==0} {
					set name1 $source
				} elseif {$i==($total_num_nodes-1)} {
					set name1 $last
				} else {
					set name1 [format %c [expr $i-1+[scan A %c]]]
				}
				#puts -nonewline "$i:$name1 -> "
				if {$j==0} {
					set name2 $source
				} elseif {$j==($total_num_nodes-1)} {
					set name2 $last
				} else {
					set name2 [format %c [expr $j-1+[scan A %c]]]
				}
				#puts -nonewline "$j:$name2 : "
				
				if {[info exists connection($name1,$name2)]} {
					if {$connection($name1,$name2)==1} {
						append s 1
					}
					#puts "connection exists"
				} else {
					append s 0
					#puts "no connection"
				}
				if {$name2!=$last} {append s " "}
			}
			puts $slackfile $s
		}
	   
	   #sort array and save values of nodes inside the file
		puts "start sorting"
		set L [lsort -increasing -ascii [array names slacks]]
		set s ""
	   foreach pl $L {
			puts "$pl,$slacks($pl)"
			append s $slacks($pl) " "
	   }
	   puts -nonewline $slackfile $s
	   
       close $slackfile
       
       # Step 2: Before proceeding to the further step, Modify your completed C program in part-1 such that
		 # it can read the adjacency matrix from the file "inputs_dfs.txt" rather than the user input.
		 # Modify your completed C program in part-1 such that it writes the arrival time, required time and
	         # slack to a file "results.txt"
              
       # Step 3: Add TCL code below to Compile and Run the C program by using the Tcl command 'exec'
       set exe_file_name "dfs_getresults"
	   set data_file_name "inputs_dfs.txt"
	   set resultsfile_name "results.txt"
       exec $exe_file_name $data_file_name $resultsfile_name;#>@stdout 2>@stderr
	   
       # Step 4: Add TCL commands below such that it reads the "outputs.txt" and displays them on the Graphical User Interface
       set resultsfile [open $resultsfile_name r]
	   set file_data [read $resultsfile]
	   close $resultsfile
	   
	   global arrival
	   global required
	   global slack
	   set lines [split $file_data "\n"]
	   set i 0
	   foreach line $lines {
			set segments [split $line " "]
			set i 0
			set name ""
			foreach segment $segments {
				if {$i==0} {
					set name $segment
					if {$name==0} {set name $source
					} elseif {$name==1} {set name $last}
				} elseif {$i==1} {
					set arrival($name) $segment
				} elseif {$i==2} {
					set required($name) $segment
				} elseif {$i==3} {
					set slack($name) $segment
				}
				incr i
			}
	   }
	   
	   set slacks(PI) 0 ;#these variables where unset to order the array slacks
	   set slacks(PO) 0
		
		
	   # global instrSlackText
	   # set instrSlackText "times are shown over nodes: arrival/required=>slack"
	   update_canvas
	   update_notification "nodes in red have slack=0 and are in the critical path"
	   
	   #show results on canvas
	    set resultsText ""
	    append resultsText "Final Results:\nNodes     Delay     Arrival    Required    Slack\n"
		set key "PI"
		append resultsText $key;#[format {%3s} [expr {$key}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$slacks($key)}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$arrival($key)}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$required($key)}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$slack($key)}]]
		append resultsText "\n"
	    foreach key $L {
			if {$key!="PI" && $key!="PO"} {
				append resultsText $key;#[format {%3s} [expr {$key}]]
				append resultsText "  \t  "
				append resultsText [format {%3d} [expr {$slacks($key)}]]
				append resultsText "  \t  "
				append resultsText [format {%3d} [expr {$arrival($key)}]]
				append resultsText "  \t  "
				append resultsText [format {%3d} [expr {$required($key)}]]
				append resultsText "  \t  "
				append resultsText [format {%3d} [expr {$slack($key)}]]
				append resultsText "\n"
			}
		}
		set key "PO"
		append resultsText $key;#[format {%3s} [expr {$key}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$slacks($key)}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$arrival($key)}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$required($key)}]]
		append resultsText "  \t  "
		append resultsText [format {%3d} [expr {$slack($key)}]]
		append resultsText "\n"
		.c1 itemconfigure resultstag -text $resultsText
		puts $resultsText
	   
   }

main

#initializing
set slacks(PI) 0
set slacks(PO) 0

#initializing for testing
#add_child $source
#join_nodes A $last
#set slacks(A) 2

update_canvas



